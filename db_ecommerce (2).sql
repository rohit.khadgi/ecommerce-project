-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2020 at 07:41 AM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_brand`
--

CREATE TABLE `tbl_brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `brand_description` varchar(255) NOT NULL,
  `brand_slug` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_brand`
--

INSERT INTO `tbl_brand` (`brand_id`, `brand_name`, `brand_description`, `brand_slug`) VALUES
(1, 'HP', 'all', 'hp'),
(2, 'Samsung', 'all', 'samsung'),
(3, 'Apple', 'all', 'apple');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `cat_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_description` longtext NOT NULL,
  `category_slug` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cat_id`, `category_name`, `category_description`, `category_slug`) VALUES
(1, 'Electronic', 'all electronic items only...', 'electronic'),
(2, 'Mobile Phone', 'all mobile phones', 'mobile-phone'),
(3, 'Clothes', 'clothes', 'clothes');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_check`
--

CREATE TABLE `tbl_check` (
  `order_id` int(11) NOT NULL,
  `b_name` varchar(255) NOT NULL,
  `b_street` varchar(255) NOT NULL,
  `b_city` varchar(255) NOT NULL,
  `b_country` varchar(255) NOT NULL,
  `b_phone` varchar(255) NOT NULL,
  `b_email` varchar(255) NOT NULL,
  `s_name` varchar(255) NOT NULL,
  `s_street` varchar(255) NOT NULL,
  `s_city` varchar(255) NOT NULL,
  `s_country` varchar(255) NOT NULL,
  `s_phone` varchar(255) NOT NULL,
  `s_email` varchar(255) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `product_quantity` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_check`
--

INSERT INTO `tbl_check` (`order_id`, `b_name`, `b_street`, `b_city`, `b_country`, `b_phone`, `b_email`, `s_name`, `s_street`, `s_city`, `s_country`, `s_phone`, `s_email`, `product_id`, `product_price`, `product_quantity`, `amount`, `status`) VALUES
(13, 'Rohit Shahi', 'kalank', 'kathmandu', 'Nepal', '9843615382', 'roytshae@gmail.com', 'Rohit Shahi', 'Ranzit Colony', 'kathmandu', 'Nepal', '9843615382', 'futikiqas@mailinator.net', 'a:1:{i:0;i:89;}', '', 'a:1:{i:0;s:1:\"1\";}', '90000', '3'),
(15, 'Jolene Cotton', 'Sunt officiis eiusmo', 'Consequuntur laboris', 'Quia necessitatibus', '172', 'jomokyku@mailinator.com', 'Jolene Cotton', 'Sunt officiis eiusmo', 'Consequuntur laboris', 'Quia necessitatibus', '172', 'jomokyku@mailinator.com', 'a:1:{i:0;i:88;}', '', 'a:1:{i:0;s:1:\"2\";}', '100000', '2');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_home`
--

CREATE TABLE `tbl_home` (
  `home_id` int(11) NOT NULL,
  `page_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_home`
--

INSERT INTO `tbl_home` (`home_id`, `page_id`) VALUES
(1, '6'),
(1, '6');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `order_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_id`, `name`, `address`, `contact`, `product_name`, `product_price`, `product_image`, `quantity`, `amount`) VALUES
(7, 'Tanner Wood', 'Rerum maiores quaera', 'Doloremque natus eu', 'Hp Laptop', '50000-/', 'image_390Lkom5sTe.jpg', '2', 100000),
(8, 'Dennis Jacobs', 'Voluptatum voluptas', 'Minus culpa adipisc', 'Hp Laptop', '50000-/', 'image_390Lkom5sTe.jpg', '2', 100000),
(9, 'Dennis Jacobs', 'Voluptatum voluptas', 'Minus culpa adipisc', 'Iphone X', '90000-/', 'image_72HhV3gMK9y.jpg', '2', 180000),
(10, 'Baxter Riddle', 'Unde non voluptatem', 'Error architecto dol', 'Samsung S10', '38000/-', 'image_QALveDnJDAP.jpg', '1', 38000),
(11, 'Baxter Riddle', 'Unde non voluptatem', 'Error architecto dol', 'Iphone X', '90000-/', 'image_72HhV3gMK9y.jpg', '2', 180000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_page`
--

CREATE TABLE `tbl_page` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_content` longtext NOT NULL,
  `page_image` varchar(255) NOT NULL,
  `page_menu_order` varchar(255) NOT NULL,
  `show_in_menu` varchar(255) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_templates` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_page`
--

INSERT INTO `tbl_page` (`page_id`, `page_title`, `page_content`, `page_image`, `page_menu_order`, `show_in_menu`, `page_slug`, `page_templates`) VALUES
(3, 'electronic', 'electronic', '', '2', 'yes', 'electronic', 'product-category'),
(4, 'Mobile phone', 'all phones', '', '5', 'no', 'mobile-phone', 'product-category'),
(6, 'Home', 'HOME PAGE', '', '1', 'yes', 'home', 'default'),
(7, 'hp', 'al product about hp brand', '', '7', 'no', 'hp', 'product-brand'),
(8, 'samsung', 'samsung mobile phone', '', '7', 'no', 'samsung', 'product-brand'),
(9, 'apple', 'apple product', '', '9', 'no', 'apple', 'product-brand'),
(10, 'Cart', 'Record about cart items', '', '3', 'no', 'cart', 'product-cart'),
(11, 'check-out', 'check out', '', '5', 'no', 'check-out', 'check-out'),
(12, 'Welcome', 'Hello Welcome to Ecommerce Site', 'image_vXP9WTs7aG.png', '3', 'yes', 'contact', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `p_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_description` longtext NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `cat_id` varchar(255) NOT NULL,
  `tag_id` varchar(255) NOT NULL,
  `brand_id` varchar(255) NOT NULL,
  `product_gallery` varchar(255) NOT NULL,
  `product_slug` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`p_id`, `product_name`, `product_image`, `product_description`, `product_price`, `cat_id`, `tag_id`, `brand_id`, `product_gallery`, `product_slug`) VALUES
(85, 'Samsung mobile', 'image_DeuqZ1i6WKS.jpg', '<p><strong>Buy Samsung Galaxy M20 64GB</strong></p>\r\n', '11499/-', '2', '1', '2', 'a:1:{i:0;s:17:\"image-YiV6l3M.jpg\";}', 'samsung-mobile'),
(87, 'samsung Tv', 'image_51uAMaL6y8O.jpg', '<p><strong>Samsung Led TV</strong></p>\r\n', '50000-/', '1', '1', '2', 'a:1:{i:0;s:17:\"image-YRRyQs1.jpg\";}', 'samsung-mobile-1'),
(88, 'Hp Laptop', 'image_390Lkom5sTe.jpg', '<p><strong>Hp Laptop</strong></p>\r\n', '50000-/', '1', '1,3', '1', 'a:1:{i:0;s:17:\"image-Fb0z65k.jpg\";}', 'hp-laptop'),
(89, 'Iphone X', 'image_72HhV3gMK9y.jpg', '<p><strong>Latest Iphone X&nbsp;</strong></p>\r\n\r\n<p><strong>Made by Apple Company</strong></p>\r\n\r\n<p><strong>OS:IOS 12 VERSION</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n', '90000-/', '2', '1', '3', 'a:2:{i:0;s:17:\"image-Xi5x0Xs.jpg\";i:1;s:17:\"image-pxaBp0f.jpg\";}', 'iphone-x'),
(90, 'Samsung S10', 'image_QALveDnJDAP.jpg', '<p><strong>Samsung S10 Mobile Phone</strong></p>\r\n', '38000/-', '2', '1', '2', 'a:1:{i:0;s:16:\"image-La9Lp5.jpg\";}', 'samsung-s10'),
(91, 'Mac Computer', 'image_x0ckakExt.jpg', '<p><strong>Apple brand Computer Mac</strong></p>\r\n', '200000/-', '1', '1', '3', 'a:1:{i:0;s:17:\"image-vemg5H0.jpg\";}', 'mac-computer');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_group_relation`
--

CREATE TABLE `tbl_product_group_relation` (
  `group_id` int(11) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `group_relation_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product_group_relation`
--

INSERT INTO `tbl_product_group_relation` (`group_id`, `product_id`, `group_name`, `group_relation_id`) VALUES
(107, '84', 'Category', '1'),
(108, '84', 'Brand', '2'),
(109, '84', 'Tag', '1'),
(110, '84', 'Tag', '3'),
(111, '85', 'Category', '2'),
(112, '85', 'Brand', '2'),
(113, '85', 'Tag', '1'),
(114, '86', 'Category', '1'),
(115, '86', 'Brand', '2'),
(116, '86', 'Tag', '1'),
(117, '87', 'Category', '1'),
(118, '87', 'Brand', '2'),
(119, '87', 'Tag', '1'),
(120, '88', 'Category', '1'),
(121, '88', 'Brand', '1'),
(122, '88', 'Tag', '1'),
(123, '88', 'Tag', '3'),
(124, '89', 'Category', '2'),
(125, '89', 'Brand', '3'),
(126, '89', 'Tag', '1'),
(127, '90', 'Category', '2'),
(128, '90', 'Brand', '2'),
(129, '90', 'Tag', '1'),
(130, '91', 'Category', '1'),
(131, '91', 'Brand', '3'),
(132, '91', 'Tag', '1'),
(133, '92', 'Category', '2'),
(134, '92', 'Brand', '3'),
(135, '92', 'Tag', '3'),
(136, '93', 'Category', '2'),
(137, '93', 'Brand', '3'),
(138, '93', 'Tag', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `status_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`status_id`, `status`) VALUES
(1, 'Pending'),
(2, 'Processing'),
(3, 'Delivered');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tag`
--

CREATE TABLE `tbl_tag` (
  `tag_id` int(11) NOT NULL,
  `tag_name` varchar(255) NOT NULL,
  `tag_description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tag`
--

INSERT INTO `tbl_tag` (`tag_id`, `tag_name`, `tag_description`) VALUES
(1, 'check', 'tag'),
(3, 'test', 'testimng');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `username`, `password`) VALUES
(2, 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `tbl_check`
--
ALTER TABLE `tbl_check`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_page`
--
ALTER TABLE `tbl_page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `tbl_product_group_relation`
--
ALTER TABLE `tbl_product_group_relation`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `tbl_tag`
--
ALTER TABLE `tbl_tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_check`
--
ALTER TABLE `tbl_check`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_page`
--
ALTER TABLE `tbl_page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `tbl_product_group_relation`
--
ALTER TABLE `tbl_product_group_relation`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_tag`
--
ALTER TABLE `tbl_tag`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
