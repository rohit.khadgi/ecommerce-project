jQuery(document).ready(function () {
    //alert('test');
    
     
    $('.edit').submit(function (e) {
        e.preventDefault();
        var selector = $(this);
        var product_id = $(this).find('.pid').val();
        var quantity = $(this).find('.qty').val();
        var ajax_url = 'http://localhost/ecommerce/edit.php';
        $.ajax({
            type: 'post',
            url: ajax_url,
            data: {
                product_id: product_id,
                quantity: quantity
            },
            beforeSend: function (xhr) {
               selector.find('.ajax-loader').show();
            },
            success: function (res) {
                selector.find('.ajax-loader').hide();
                res = $.parseJSON(res);
                //console.log(res);
                if(res.status == 200){
                   
                    $('.edit-message').html(res.message);
                }
            }
        });
    });
});
