jQuery(document).ready(function () {
    var site_url = $('#siteurl').val();
      $('#c').submit(function(e){
        var name = $('input[name="name"]').val();
        var contact = $('input[name="Contact"]').val();
         var address = $('input[name="address"]').val();
        if(name == '' || contact == '' || address== '' ){
            e.preventDefault();
            $('.message').html('Fill the form please!!!.');
        }
       
    });
    
    $('#c input[type="text"],#c input[type="text"]').keyup(function(){
       $('.message').html(''); 
    });

     $(".check-out").click(function(){
        //alert("test");
        if($(this).is(':checked')){
        var name = $('input[name="name"]').val();
        var street = $('input[name="street"]').val();
        var city = $('input[name="city"]').val();
        var country = $('input[name="country"]').val();
        var phonenumber = $('input[name="number"]').val();
        var email = $('input[name="email"]').val();
        //set the variable in lower form with vars set above
       $("#n1").val(name);
          $("#s1").val(street);
          $("#c1").val(city);
          $("#cc1").val(country);
          $("#p1").val(phonenumber);
          $("#e1").val(email);
        }
        else{
//uncheck - clear input
        $("#n1").val("");
        $("#s1").val(""); 
        $("#c1").val("");
        $("#cc1").val("");
        $("#p1").val("");
        $("#e1").val("");
        }
    });
     
    $('.cart-form').submit(function (e) {
        e.preventDefault();
        var selector = $(this);
        var product_id = $(this).find('.pid').val();
        var quantity = $(this).find('.qty').val();
        var ajax_url = site_url+'/cart-ajax.php';    
        $.ajax({
            type: 'post',
            url: ajax_url,
            data: {
                action:'add_cart',
                product_id: product_id,
                quantity: quantity
            },
            beforeSend: function (xhr) {
               selector.find('.ajax-loader').show();
            },
            success: function (res) {
                selector.find('.ajax-loader').hide();
                res = $.parseJSON(res);
                console.log(res);
                if(res.status == 200){
                    $('.cart-total').html(res.cart_item_count);
                    $('.cart-message').html(res.message);
                }
            }
        });
    });

    $('.cart-item-edit').click(function(){
        var selector = $(this);
        var product_id = $(this).data('product-id');
        var product_qty = $(this).closest('tr').find('.qty').val();
        var ajax_url = site_url+'/cart-ajax.php';
        $.ajax({
            type: 'post',
            url: ajax_url,
            data: {
                action:'edit_cart',
                product_id: product_id,
                product_qty: product_qty
            },
            beforeSend: function (xhr) {
               selector.parent().find('.ajax-loader').show();
            },
            success: function (res) {
                selector.parent().find('.ajax-loader').hide();
                res = $.parseJSON(res);
                if(res.remove){
                  //  selector.closest('tr').remove();
                    selector.closest('tr').fadeOut(500,function(){
                        $(this).remove();
                    });
                }
                
                
            }
        });
    });
  
});
