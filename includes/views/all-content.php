<div class="product-list-wrap">
    <?php
    $select_query = $init_obj->library->select_data('tbl_product');



    foreach ($select_query as $product_row) {
        $p = $product_row['p_id'];
        ?>
        <div class="each-product clearfix">
            <div class="product-left">
                <img src="images/<?php echo $product_row['product_image']; ?>" width="120px"/><br>
            </div>
            <div class="product-right">
                <h2><?php echo $product_row['product_name']; ?></h2>

                <h3><STRONG>Price:</STRONG><?php echo $product_row['product_price']; ?></h3>
                <?php $cat_id = $product_row['cat_id'];
                ?>
                <?php $brand_id = $product_row['brand_id'];
                ?>
                <?php
                $tag = explode(",", $product_row['tag_id']);
                // print_r($tag);
                //echo count($tag);
                // $last_index=count($tag)-1;
                //echo  $last_index;
                ?>

                <button><a href="<?php echo SITEURL . '?p_id=' . $product_row['p_id']; ?>">View Detail</a></button><br>
                <?php
                $query = $init_obj->library->select_data('tbl_category', array(), array('cat_id' => $cat_id));
                //print_r($query);
                foreach ($query as $cat) {
                    ?>
                    <h4>Category:<a href="<?php echo SITEURL . '?cat_id=' . $product_row['cat_id']; ?> "><?php echo $cat['category_name']; ?></h4></a>
                    <?php
                }
                ?>
                <?php
                $query = $init_obj->library->select_data('tbl_brand', array(), array('brand_id' => $brand_id));
                foreach ($query as $cat) {
                    ?>
                    <h4>Brand: <a href="<?php echo SITEURL . '?brand_id=' . $product_row['brand_id']; ?>"><?php echo $cat['brand_name']; ?></h4></a>
                    <?php
                }
                ?>
                <?php
                foreach ($tag as $t) {
                    $id = $t;
                    $query = $init_obj->library->select_data('tbl_tag', array(), array('tag_id' => $id));
                    foreach ($query as $tag) {
                        ?>
                        <h4>Tag:<a href="<?php echo SITEURL . '?tag_id=' . $tag['tag_id']; ?>"><?php echo $tag['tag_name']; ?> </h4></a>

                        <?php
                    }
                    ?>

                    <?php
                }
                ?>
                <form method="post" class="cart-form">
                    Qty:<input type="number" name="qty" min="1" max="5" class="qty">
                    <input type="submit" value="Add to cart">
                    <img src="<?php echo SITEURL . '/images/ajax-loader.gif' ?>" class="ajax-loader"/>
                    <input type="hidden" name="p_id" value="<?php echo $p; ?>" class="pid">
                </form>

            </div>
        </div>
        <?php
    }
    ?>


</div>

