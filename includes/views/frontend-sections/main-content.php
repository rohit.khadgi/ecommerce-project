<?php
if ( !empty( $_GET['page'] ) ) {
    $page_slug = $init_obj->library->sanitize_input( $_GET['page'] );
    $page_rows = $init_obj->library->select_data( 'tbl_page', array(), array( 'page_slug' => $page_slug ) );
    if ( !empty( $page_rows ) ) {
        $page_row = $page_rows[0];
        $page_template = $page_row['page_template'];
        include('includes/views/templates/' . $page_template . '-template.php');
    } else {
        include('includes/views/404.php');
    }
}
if (!empty($_GET['page']) && $_GET['page'] == 'cart') {
    include('includes/views/cart-items.php');
} else {


    if (!empty($_GET['cat_id']) && !empty($_GET['brand_id'])) {

        $cat_id = $_GET['cat_id'];
        $brand_id = $_GET['brand_id'];


        $select_query = $init_obj->library->select_data('tbl_product', array(), array('cat_id' => $cat_id, 'brand_id' => $brand_id), 'and', 'p_id', 'desc');
        if (!empty($select_query)) {
            include('includes/views/filter-product.php');
        } else {

            include('includes/views/404.php');
        }
    }
    if (empty($_GET['cat_id']) && empty($_GET['p_id']) && empty($_GET['brand_id']) && empty($_GET['tag_id'])) {
        include('includes/views/all-content.php');
    }


    if (!empty($_GET['p_id'])) {
        $p_id = $init_obj->library->sanitize_input($_GET['p_id']);
        $product_rows = $init_obj->library->select_data('tbl_product', array(), array('p_id' => $p_id));
        if (!empty($product_rows)) {
            $product_row = $product_rows[0];
            include('includes/views/product-detail.php');
        } else {
            include('includes/views/error.php');
        }
    }

    if (!empty($_GET['cat_id']) && empty($_GET['brand_id']) && empty($_GET['tag_id'])) {
        $cat_id = $init_obj->library->sanitize_input($_GET['cat_id']);
        $product_rows = $init_obj->library->select_data('tbl_product', array(), array('cat_id' => $cat_id));
        if (!empty($product_rows)) {
            $cat_row = $product_rows;
            include('includes/views/category-detail.php');
        } else {
            include('includes/views/error.php');
        }
    }
    if (!empty($_GET['brand_id']) && empty($_GET['cat_id']) && empty($_GET['tag_id'])) {
        $brand_id = $init_obj->library->sanitize_input($_GET['brand_id']);
        $product_rows = $init_obj->library->select_data('tbl_product', array(), array('brand_id' => $brand_id));
        if (!empty($product_rows)) {
            $brand_row = $product_rows;
            include('includes/views/brand-detail.php');
        } else {
            include('includes/views/error.php');
        }
    }
    if (!empty($_GET['tag_id']) && empty($_GET['cat_id']) && empty($_GET['brand_id'])) {
        $tag_id = $init_obj->library->sanitize_input($_GET['tag_id']);
        $product_rows = $init_obj->library->select_data('tbl_product', array(), array('tag_id' => $tag_id));
        if (!empty($product_rows)) {
            $tag_row = $product_rows;
            include('includes/views/tag-detail.php');
        } else {
            include('includes/views/error.php');
        }
    }
    if (!empty($_GET['p_id']) && !empty($_GET['qty'])) {
        $p_id = $init_obj->library->sanitize_input($_GET['p_id']);
        $product_rows = $init_obj->library->select_data('tbl_product', array(), array('p_id' => $p_id));
        if (!empty($product_rows)) {
            $_SESSION['product'] = $product_rows;

            include('includes/views/cartprocess.php');
        } else {
            include('includes/views/error.php');
        }
    }
}