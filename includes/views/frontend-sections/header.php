<?php include('admin/includes/classes/class-init.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Online Shop</title>
        <script type="text/javascript" src="<?php echo SITEURL . '/js/jquery.js' ?>"></script>
        <script type="text/javascript" src="<?php echo SITEURL . '/js/scripts.js' ?>"></script>
        <link rel="stylesheet" href="style/front.css">
    </head>
    <body>
        <input type="hidden" id="siteurl" value="<?php echo SITEURL; ?>"/>
        <div class="header">
            <h1><a href="<?php echo SITEURL; ?>">Online Shop</a></h1>
            <div class="cart-wrap">
                <span class="cart-total"><?php echo (!empty($_SESSION['cart_items'])) ? count($_SESSION['cart_items']) : 0; ?></span>
                <a href="<?php echo SITEURL . '/index.php?page=cart'; ?>"><img src="images/cart1.jpg"></a>
            </div>
            <div class="clear"></div>
        </div>

        <div class="sidebar">
            <form>
                <input type="submit"  style="margin: 55px;" value="Filter"/>

                <h5 style="margin: 15px;">Category:</h5>
                <select style="margin: 15px;" name="cat_id">
                    <option value="">Choose Category</option>
                    <?php
                    
                                        
                    $selected_cat_id = (!empty($_GET['cat_id'])) ? $_GET['cat_id'] : '';
                    $category = $init_obj->library->select_data('tbl_category', array(), array());
                    //echo "<pre>";
                    //print_r($category);
                    // echo "</pre>";
                    //die();
                    end($category);
                    $key = key($category);


                    for ($i = 0; $i <= $key; $i++) {
                        ?>
                        <option value="<?php echo $category[$i]['cat_id']; ?>" <?php echo ($selected_cat_id == $category[$i]['cat_id']) ? 'selected="selected"' : ''; ?>><?php echo $category[$i]['category_name']; ?></option>
                        <?php
                    }
                    ?>

                </select>

                <h5 style="margin: 15px;">Brand:</h5>
                <select style="margin: 15px;" name="brand_id">
                    <option value="">Choose Brand</option>
                    <?php
                    $selected_brand_id = (!empty($_GET['brand_id'])) ? $_GET['brand_id'] : '';
                    $brand = $init_obj->library->select_data('tbl_brand', array(), array());
                    //echo "<pre>";
                    //print_r($category);
                    // echo "</pre>";
                    //die();
                    end($brand);
                    $key = key($brand);


                    for ($i = 0; $i <= $key; $i++) {
                        ?>
                        <option value="<?php echo $brand[$i]['brand_id']; ?>" <?php
                        echo ($selected_brand_id == $brand[$i]['brand_id']) ?
                                'selected="selected"' : '';
                        ?>><?php echo $brand[$i]['brand_name']; ?></option><br>
                                <?php
                            }
                            ?>

                </select>

                <h5 style="margin: 15px;">Tags:</h5>
                <?php
                $checked_tag_id = (!empty($_GET['tag_id'])) ? $_GET['tag_id'] : '';
                $tag = $init_obj->library->select_data('tbl_tag', array(), array());
                //echo "<pre>";
                //print_r($category);
                // echo "</pre>";
                //die();
                end($tag);
                $key = key($tag);


                for ($i = 0; $i <= $key; $i++) {
                    ?>
                    <input type="checkbox" name="tag[]" value="<?php echo $tag[$i]['tag_id'] ?>" <?php echo ($checked_tag_id == $tag[$i]['tag_id']) ? 'checked="checked"' : ''; ?>><?php echo $tag[$i]['tag_name']; ?>
                    <?php
                }
                ?>
            </form>
        </div>

    </body>
</html>