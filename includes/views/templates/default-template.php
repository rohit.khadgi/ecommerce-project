<section class="about-area">
    <div class="container">

        <div class="row">
            <div class="col-12">
                <h2><?php echo $page_row['page_title']; ?></h2>
            </div>
        </div>

        <div class="row">
            <?php if ( !empty( $page_row['page_image'] ) ) {
                ?>
                <div class="col-12 col-lg-12">
                    <p><img src="<?php echo SITEURL . '/images/' . $page_row['page_image']; ?>"/></p>
                </div>
                <?php
            }
            ?>
            <div class="col-12 col-lg-12">
                <?php echo $page_row['page_content']; ?>
            </div>
        </div>


    </div>
</section>
