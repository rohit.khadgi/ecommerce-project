<?php 
include('admin/includes/classes/class-init.php');
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre>";
// die();
if(!empty($_REQUEST['action'])){
	$action = $_REQUEST['action'];
	switch($action){
		case 'edit_cart':
		if(empty($_REQUEST['product_id'])){
			$response = array('status'=>403,'message'=>'Required parameters missing.');
		}
		$product_id = intval($_POST['product_id']);
		$product_qty = intval($_POST['product_qty']);
		if(empty($product_qty)){
			unset($_SESSION['cart_items'][$product_id]);
			$response['status'] = 200;
			$response['remove'] = true;
			$response['message'] = 'Product removed from cart.';
		}else{
			$_SESSION['cart_items'][$product_id]['quantity'] = $product_qty;	
			$response['status'] = 200;
			$response['message'] = 'Cart updated successfully';
		}
		
		break;
		case 'add_cart':
		if (!empty($_POST['product_id']) && $_POST['quantity']) {
    
    $product_id = $init_obj->library->sanitize_input($_POST['product_id']);
    $quanity = $init_obj->library->sanitize_input($_POST['quantity']);
    if (isset($_SESSION['cart_items'][$product_id])) {
        $cart_product_quantity = $_SESSION['cart_items'][$product_id]['quantity'];
        $new_quantity = $cart_product_quantity + $quanity;
        $cart_item_array = array('quantity' => $new_quantity);
    } else {
        $cart_item_array = array('quantity' => $quanity);
    }
    $_SESSION['cart_items'][$product_id] = $cart_item_array;
    $response = array('status' => 200, 'message' => 'Item added successfully.', 'cart_item_count' => count($_SESSION['cart_items']));

   
} 
		break;
	}
	echo json_encode($response);
	die();
}else{
	die('Unauthorized access');
}