<?php
if ( !empty( $_GET['page'] ) ) {
    $page_slug = $init_obj->library->sanitize_input( $_GET['page'] );
    $page_rows = $init_obj->library->select_data( 'tbl_page', array(), array( 'page_slug' => $page_slug ) );
    if ( !empty( $page_rows ) ) {
        $page_row = $page_rows[0];
        $page_template = $page_row['page_templates'];
        include('include/views/templates/' . $page_template . '-template.php');
    } else {
        include('include/views/404.php');
    }
}
else if ( !empty( $_GET['page1'] ) && !empty($_GET['page2']) ) {
    $page_slug1 = $init_obj->library->sanitize_input( $_GET['page1'] );
     $page_slug2 = $init_obj->library->sanitize_input( $_GET['page2'] );
    $page_rows = $init_obj->library->select_data( 'tbl_page', array(), array( 'page_slug' => $page_slug2 ) );
    if ( !empty( $page_rows ) ) {
        $page_row = $page_rows[0];
        $page_template = $page_row['page_templates'];
        include('include/views/templates/' . $page_template . '-template.php');
    } else {
        include('include/views/404.php');
    }
}
 else if ( !empty( $_GET['product'] ) ) {
    $product_slug = $init_obj->library->sanitize_input( $_GET['product'] );
    $product_row = $init_obj->library->select_data( 'tbl_product', array(), array( 'product_slug' => $product_slug ) );
    if ( !empty( $product_row ) ) {
        $product_row = $product_row[0];
        include('include/views/product-details.php');
    } else {
        include('include/views/404.php');
    }
} 
else if ( !empty( $_GET['news'] ) ) {
    $news_slug = $init_obj->library->sanitize_input( $_GET['news'] );
    $news_rows = $init_obj->library->select_data( 'tbl_news', array(), array( 'news_slug' => $news_slug ) );
    if ( !empty( $news_rows ) ) {
        $news_row = $news_rows[0];
        include('includes/views/news-detail.php');
    } else {
        include('includes/views/404.php');
    }
}
else {
    include('include/views/frontend-section/home-main-content.php');
}
?>