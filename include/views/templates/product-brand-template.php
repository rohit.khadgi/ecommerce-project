  

<section>
    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="left-sidebar">
            <h2>Category</h2>
            <div class="panel-group category-products" id="accordian"><!--category-productsr-->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">

                     <?php 
                    $pages = $init_obj->library->select_data( 'tbl_page', array(), array( 'page_templates'=>'product-category' ), 'and', 'page_menu_order', 'asc' );

                                        if ( !empty( $pages ) ) {
                                                
                                            foreach ( $pages as $page ) {
                                                //  print_r($page);
                                                // die();
                                                $url = SITEURL . '/index.php?page=' . $page['page_slug'];
                                                ?>
                                                <a href="<?php echo $url; ?>" ><?php echo $page['page_title']; ?></a><br>
                                                <?php
                                            }
                                        }
                                        ?>
                    </a>
                  </h4>
                </div>
             
              </div>
            
              
           
           
            </div><!--/category-productsr-->
          
            <div class="brands_products"><!--brands_products-->
              <h2>Brands</h2>
              <div class="brands-name">
                <ul class="nav nav-pills nav-stacked">
                  <li><a href=""> <span class="pull-right"></span>
                     <?php 
                    $pages = $init_obj->library->select_data( 'tbl_page', array(), array('page_templates'=>'product-brand' ), 'and', 'page_menu_order', 'asc' );

                                        if ( !empty( $pages ) ) {
                                                
                                            foreach ( $pages as $page ) {
                                                //  print_r($page);
                                                // die();
                                                $url =  SITEURL . '/index.php?page1=' .$page_slug1 .'&&'.'page2='.$page['page_slug'];
                                                ?>
                                                <a href="<?php echo $url; ?>" ><?php echo $page['page_title']; ?></a><br>
                                                <?php
                                            }
                                        }
                                        ?>


                  </a></li>
                 
                </ul>
              </div>
            </div><!--/brands_products-->
            
        <!--     <div class="price-range">price-range-->
             <!--  <h2>Price Range</h2>
              <div class="well">
                 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
                 <b>$ 0</b> <b class="pull-right">$ 600</b>
              </div>
            </div> -->
            <div class="shipping text-center"><!--shipping-->
              <img src="images/home/shipping.jpg" alt="" />
            </div><!--/shipping-->
            
          </div>
        </div>
   
          <div class="col-sm-9 padding-right">
          <div class="features_items"><!--features_items-->
            <h2 class="title text-center"><?php $product_brand_slug = $page_slug2; echo strtoupper($product_brand_slug);?> Product</h2>
            <div class="col-sm-4">
              <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                       <?php 
                      $product_category_slug = $page_slug1; 
                     $product_brand_slug = $page_slug2;

                    $brand_product = $init_obj->library->get_product_by_brand_with_category($product_brand_slug,$product_category_slug);

                    if ( !empty( $brand_product ) ) {
                        foreach ( $brand_product as $brand_product ) {
                          $p = $brand_product['p_id'];
                            ?>
                            <?php if ( !empty( $brand_product['product_image'] ) ) { ?>
                    <a href="<?php echo SITEURL . '/index.php?product=' . $brand_product['product_slug']; ?>"><img src="<?php echo SITEURL . '/images/' . $brand_product['product_image']; ?>" alt=""></a>
                    <h2><?php echo $brand_product['product_price'];?></h2>
                    <p><?php echo $brand_product['product_name'];?></p>
                    <a href="<?php echo SITEURL . '/index.php?product=' . $brand_product['product_slug']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>View Detail</a>
                    <form action="" method="post" class="cart-form">
                       Qty:<input type="number" name="qty" min="1" max="5" class="qty"><br>
                        <button class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                        <img src="images/ajax-loader.gif" class="ajax-loader" style="display: none; width: 50px; height: 50px;">
                        <input type="hidden" name="p_id" value="<?php echo $p; ?>" class="pid">
                        </form>
                          <?php
        }
      }
    }
    else
    {
      ?>
      <h1>Sorry!!! <br> <?php echo strtoupper($product_brand_slug); ?> Brand Product Not Found!!! <br> in <br><?php echo strtoupper( $product_category_slug);?> <br> Category</h1>
      <?php 
    }
      ?>
                      </div>

                    </div>

                </div>
               


              </div>

          
         
            </div>
               
             
         
       
         
            
   
             </div>

           
            
            
         
            
        
        
            
           <!--  <ul class="pagination">
              <li class="active"><a href="">1</a></li>
              <li><a href="">2</a></li>
              <li><a href="">3</a></li>
              <li><a href="">&raquo;</a></li>
            </ul> -->
          </div><!--features_items-->
        </div>
      </div>
    </div>
  </section>