<?php

//print_r($_SESSION);
?>

<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											
									
									<?php 
									  $pages = $init_obj->library->select_data( 'tbl_page', array(), array( 'page_templates'=>'product-category'), 'and', 'page_menu_order', 'asc' );

                                        if ( !empty( $pages ) ) {
                                                
                                            foreach ( $pages as $page ) {
                                                //  print_r($page);
                                                // die();
                                                $url = SITEURL . '/index.php?page=' . $page['page_slug'];
                                                ?>
                                                <a href="<?php echo $url; ?>" ><?php echo $page['page_title']; ?></a><br>
                                                <?php
                                            }
                                        }
                                        ?>
										</a>

									</h4>
								</div>
							
							</div>
							
				
						</div><!--/category-products-->
					<!-- 
						<div class="brands_products">
							<h2>Brands</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">
									<li><a href="#"> <span class="pull-right">(50)</span>Acne</a></li>
									<li><a href="#"> <span class="pull-right">(56)</span>Grüne Erde</a></li>
									<li><a href="#"> <span class="pull-right">(27)</span>Albiro</a></li>
									<li><a href="#"> <span class="pull-right">(32)</span>Ronhill</a></li>
									<li><a href="#"> <span class="pull-right">(5)</span>Oddmolly</a></li>
									<li><a href="#"> <span class="pull-right">(9)</span>Boudestijn</a></li>
									<li><a href="#"> <span class="pull-right">(4)</span>Rösch creative culture</a></li>
								</ul>
							</div>
						</div> --><!--/brands_products-->
						
					<!-- 	<div class="price-range">
							<h2>Price Range</h2>
							<div class="well text-center">
								 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
								 <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
							</div>
						</div> --><!--/price-range-->
						
						<div class="shipping text-center"><!--shipping-->
							<img src="images/home/shipping.jpg" alt="" />
						</div><!--/shipping-->
					
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					    <?php 
                                $product = $init_obj->library->select_data('tbl_product',
                                    array(),
                                    array(),
                                    '',
                                    'p_id',
                                    'desc',
                                    '1',
                                        '3');

                               
                                $f1 = $product[0];
                                $f2 = $product[1];
                                $f3 = $product[2];


                              ?>
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<img src="images/<?php echo $f1['product_image'];?>" alt="" />
											<?php 
												$p = $f1['p_id'];
											?>
											<h2><?php echo $f1['product_price'];?></h2>
											<p><?php echo $f1['product_name'];?></p>
											<!-- <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a> -->
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<h2><?php echo $f1['product_price'];?></h2>
												<p><?php echo $f1['product_name'];?></p>
												 <form action="" method="post" class="cart-form">
                   					 Qty:<input type="number" name="qty" min="1" max="5" class="qty"><br>
                  					<button><a class="btn btn-default"><i class="fa fa-shopping-cart"></i>Add to cart</a></button> 
                   					
                    				<input type="hidden" name="p_id" value="<?php echo $p; ?>" class="pid">
                  					 </form>
                  					 <img src="images/ajax-loader.gif"  style="display:none;"class="ajax-loader">
                  					<p class="cart-message" style="display: none;"></p>
											</div>
										</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="<?php echo SITEURL . '/index.php?product=' . $f1['product_slug']; ?>"></i>View Detail</a></li>
										
									</ul>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="images/<?php echo $f2['product_image'];?>" alt="" />
										<?php $p = $f2['p_id']; ?>
										<h2><?php echo $f2['product_price'];?></h2>
										<p><?php echo $f2['product_name'];?></p>
										
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<h2><?php echo $f2['product_price'];?></h2>
											<p><?php echo $f2['product_name'];?></p>
											<form action="" method="post" class="cart-form">
                   					 Qty:<input type="number" name="qty" min="1" max="5" class="qty"><br>
                  					<button><a class="btn btn-default"><i class="fa fa-shopping-cart"></i>Add to cart</a></button> 
                   					
                    				<input type="hidden" name="p_id" value="<?php echo $p; ?>" class="pid">
                  					 </form>
                  					 <img src="images/ajax-loader.gif"  style="display:none;"class="ajax-loader">
                  					<p class="cart-message" style="display: none;"></p>
										</div>
									</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="<?php echo SITEURL . '/index.php?product=' . $f2['product_slug']; ?>"></i>View Detail</a>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="images/<?php echo $f3['product_image'];?>" alt="" />
										<?php $p = $f3['p_id']; ?>
										<h2><?php echo $f3['product_price'];?></h2>
										<p><?php echo $f3['product_name'];?></p>
										
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<h2><?php echo $f3['product_price'];?></h2>
											<p><?php echo $f3['product_name'];?></p>
											<form action="" method="post" class="cart-form">
                   					 Qty:<input type="number" name="qty" min="1" max="5" class="qty"><br>
                  					<button><a class="btn btn-default"><i class="fa fa-shopping-cart"></i>Add to cart</a></button> 
                   					
                    				<input type="hidden" name="p_id" value="<?php echo $p; ?>" class="pid">
                  					 </form>
                  					 <img src="images/ajax-loader.gif"  style="display:none;"class="ajax-loader">
                  					<p class="cart-message" style="display: none;"></p>
										</div>
									</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="<?php echo SITEURL . '/index.php?product=' . $f3['product_slug']; ?>"></i>View Detail</a>
									</ul>
								</div>
							</div>
						</div>
				
						
					</div><!--features_items-->
					
					<div class="category-tab"><!--category-tab-->
						<div class="col-sm-12">
							<?php 
								$brand = $init_obj->library->select_data('tbl_brand');
								$b1 = $brand[0];
								$b2 = $brand[1];
								$b3 = $brand[2];
								/*$b4 = $brand[3];
								$b5 = $brand[4];*/
							?>
							<ul class="nav nav-tabs">
								<li><a href="<?php echo SITEURL . '/index.php?brand=' . $b1['brand_id']; ?>" ><?php echo $b1['brand_name'];?></a></li>
								<li><a href="<?php echo SITEURL . '/index.php?brand=' . $b2['brand_id']; ?>"><?php echo $b2['brand_name'];?></a></li>
								<li><a href="<?php echo SITEURL . '/index.php?brand=' . $b3['brand_id']; ?>" ><?php echo $b3['brand_name'];?></a></li>
								<!-- <li><a href="#kids" data-toggle="tab"><?php echo $b4['brand_name'];?></a></li>
								<li><a href="#poloshirt" data-toggle="tab"><?php echo $b5['brand_name'];?> </a></li> -->
							</ul>
						</div>
						<div class="tab-content">
							<?php
							if(!empty($_GET['brand']))
							{
								$brand = $_GET['brand'];
							$product = $init_obj->library->select_data('tbl_product',array(),array('brand_id'=>$brand),'and');
							foreach($product as $pro)
							{
								$p = $pro['p_id'];
							?>
							<div class="tab-pane fade active in" id="tshirt" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="images/<?php echo $pro['product_image'];?>" alt=""/>
												<h2>Price:<?php echo $pro['product_price'];?></h2>
												<p><?php echo $pro['product_name'];?></p>
												 <form action="" method="post" class="cart-form">
                   					 Qty:<input type="number" name="qty" min="1" max="5" class="qty"><br>
												<button class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
												<input type="hidden" name="p_id" value="<?php echo $p; ?>" class="pid">
                  					 </form>
                  					  <img src="images/ajax-loader.gif"  style="display:none;"class="ajax-loader">
                  					<p class="cart-message" style="display: none;"></p>
											</div>
											
										</div>
									</div>
								</div>
								
							</div>
							<?php
						}
					}
						?>
							
			
				</div>
			</div>
		</div>
	</section>