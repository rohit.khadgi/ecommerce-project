<?php
//print_r($_POST);
//die();
if (!empty($_POST['product_id']) && $_POST['quantity']) {
    include('admin/includes/classes/class-init.php');
    $product_id = $init_obj->library->sanitize_input($_POST['product_id']);
    $quanity = $init_obj->library->sanitize_input($_POST['quantity']);
    if (isset($_SESSION['cart_items'][$product_id])) {
        $cart_product_quantity = $_SESSION['cart_items'][$product_id]['quantity'];
        $new_quantity = $cart_product_quantity + $quanity;
        $cart_item_array = array('quantity' => $new_quantity);
    } else {
        $cart_item_array = array('quantity' => $quanity);
    }
    $_SESSION['cart_items'][$product_id] = $cart_item_array;
    $response = array('status' => 200, 'message' => 'Item added successfully.', 'cart_item_count' => count($_SESSION['cart_items']));

    echo json_encode($response);
} 
else {
    die('Unauthorized access!!');
}

