<!DOCTYPE html>
<html>
<head>
<title>Passing Value Through Function</title>
<script type="text/javascript" src="js/jquery.js"></script>
</head>
<body>
<input type="text" id="textboxone">+
<input type="text" id="textboxtwo">=<span style="padding:0 10px;"></span>
<button id="addbutton">Add</button>
<script>
$("#addbutton").click(function(){
 
    var a=$("#textboxone").val();
    var b=$("#textboxtwo").val();
    var d=addnumbers(a,b);
    $("span").append(d)
});
function addnumbers(a,b)
{
    var c=Number(a)+Number(b);
    return c;
}
</script>
</body>
</html>