<?php

include('includes/classes/class-init.php');
$init_obj->library->check_login();

if ( isset( $_POST['add_user_submit'] ) ) {

    $username = $init_obj->library->sanitize_input( $_POST['username'] );
    $password = $init_obj->library->sanitize_input( $_POST['password'] );
    $insert_result = $init_obj->library->insert_data( 'tbl_users', array( 'username' => $username, 'password' => $password ) );
    if ( $insert_result ) {

        $init_obj->library->set_session( 'message', 'User Added Sucessful' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=user-manager' );
    }
}
else if ( isset( $_POST['edit'] ) ) {
   
   /* echo "<pre>";
    print_r($_POST);
    echo "</pre>";
    //die();*/

    $id = intval( $_POST['s_id'] );
    $o_id =intval(($_POST['o_id']));
   /* echo $id;
    echo $o_id;
    die();*/
   
    $result = $init_obj->library->update_data( 'tbl_check', array( 'status' => $id), array( 'order_id' => $o_id ) );
    if ( $result ) {
        $init_obj->library->set_session( 'message', 'Order Status Updated Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=order-manager' );
    }
} else if ( isset( $_POST['edit_user_submit'] ) ) {
    if ( empty( $_POST['user_id'] ) ) {
        print_r( $_POST );
        die();
        $init_obj->library->redirect( SITEURL . '/admin' );
    }


    $id = intval( $_POST['user_id'] );
    //echo $id;
    //die();
    $username = $init_obj->library->sanitize_input( $_POST['username'] );
    $password = $init_obj->library->sanitize_input( $_POST['password'] );
    $result = $init_obj->library->update_data( 'tbl_users', array( 'username' => $username, 'password' => $password ), array( 'user_id' => $id ) );
    if ( $result ) {
        $init_obj->library->set_session( 'message', 'Users Data Updated Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=user-manager' );
    }
} else if ( isset( $_GET['action'], $_GET['user_id'] ) && $_GET['action'] == 'delete-users' ) {


    $id = intval( $_GET['user_id'] );


    $result = $init_obj->library->delete_data( 'tbl_users', array( 'user_id' => $id ) );

    if ( $result ) {
        //echo "reach";
        //die();
        $init_obj->library->set_session( 'message', 'Users Account Deleted Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=user-manager' );
    }
} 
else if ( isset( $_GET['action'], $_GET['order_id'] ) && $_GET['action'] == 'delete-order' ) {


    $id = intval( $_GET['order_id'] );
    /*echo $id;
    die();*/

    $result = $init_obj->library->delete_data( 'tbl_check', array( 'order_id' => $id ) );
    
    if ( $result ) {
       /* echo "reach";
        die();*/
        $init_obj->library->set_session( 'message', 'Order Deleted Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=order-manager' );
    }
} 

if ( isset( $_POST['add_category'] ) ) {

    $category_name = $init_obj->library->sanitize_input( $_POST['category_name'] );
     $category_slug = $init_obj->library->generate_slug( $category_name, 'tbl_category', 'category_slug' );
    $category_description = $init_obj->library->sanitize_input( $_POST['category_description'] );
    $insert_result = $init_obj->library->insert_data( 'tbl_category', array( 'category_name' => $category_name, 'category_description' => $category_description,'category_slug'=>$category_slug ) );
    if ( $insert_result ) {

        $init_obj->library->set_session( 'message', 'Category Added Sucessful' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=category-manager' );
    }
}

if ( isset( $_POST['add_brand'] ) ) {

    $brand_name = $init_obj->library->sanitize_input( $_POST['brand_name'] );
     $brand_slug = $init_obj->library->generate_slug( $brand_name, 'tbl_brand', 'brand_slug' );
    $brand_description = $init_obj->library->sanitize_input( $_POST['brand_description'] );
    $insert_result = $init_obj->library->insert_data( 'tbl_brand', array( 'brand_name' => $brand_name, 'brand_description' => $brand_description,'brand_slug'=>$brand_slug ) );
    if ( $insert_result ) {

        $init_obj->library->set_session( 'message', 'Brand Added Sucessful' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=brand-manager' );
    }
}
if ( isset( $_POST['add_tag'] ) ) {

    $tag_name = $init_obj->library->sanitize_input( $_POST['tag_name'] );
    $tag_description = $init_obj->library->sanitize_input( $_POST['tag_description'] );
    $insert_result = $init_obj->library->insert_data( 'tbl_tag', array( 'tag_name' => $tag_name, 'tag_description' => $tag_description ) );
    if ( $insert_result ) {

        $init_obj->library->set_session( 'message', 'TAGS Added Sucessful' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=tag-manager' );
    }
}
 else if ( isset( $_POST['add_product'] ) ) {
    $product_gallery = array();
    foreach($_FILES['file_img']['name'] as $key=>$val){
        $filename_array = explode('.',$val);
            $ext = end($filename_array);
            $upload_filename = 'image-'.$init_obj->library->random_number(6).'.'.$ext;
            move_uploaded_file($_FILES['file_img']['tmp_name'][$key], '../images/'.$upload_filename);
            $product_gallery[] = $upload_filename;
    }
    /* echo "<pre>";
     print_r($_POST);
     // print_r($_FILES);
    //  print_r($product_gallery);
      echo "</pre>";
      die();*/
   /* foreach ($_FILES['file_img']['name'] as $key => $val)
    {
        $product_gallery =array();
        $filename_array explode('.', $val);
        $ext = end($filename_array);
        $upload_filename = 'image-'.$init_obj->library->random_number(5).'.'.$ext;
        move_uploaded_file(($_FILES['file_img']['tmp_name']['$key'],'../images/'.$upload_filename);
        $product_gallery[] = $upload_filename;
    }*/
    $product_name = $init_obj->library->sanitize_input( $_POST['product_name'] );
    $product_slug = $init_obj->library->generate_slug( $product_name, 'tbl_product', 'product_slug' );
    $product_description = $_POST['product_description'];
    $product_price = $init_obj->library->sanitize_input( $_POST['product_price'] );
    $cat_id = $init_obj->library->sanitize_input( $_POST['cat_id'] );
    $tag_id = implode(',' ,$_POST['tag']);
    $brand_id =  $init_obj->library->sanitize_input( $_POST['brand_id'] );
    // $gallery = implode(',' ,$_POST['file_img']);
    $upload_filename = $init_obj->library->insert_image();
    $product_insert = $init_obj->library->insert_data('tbl_product', array( 'product_name' => $product_name,
        'product_description' => $product_description,
        'product_price' => $product_price,
        'cat_id' => $cat_id,
        'product_image' => $upload_filename,
        'product_slug' => $product_slug,
        'brand_id' => $brand_id,
        'tag_id' => $tag_id,
        'product_gallery' =>serialize($product_gallery) ) );
        
         $product_id=$init_obj->library->insert_id();
         /*echo "$product_id";
         die();*/
    if ( $product_insert ) {
       
       
       /* echo $cat_name;
        die(); */
        $init_obj->library->insert_data('tbl_product_group_relation', array('product_id'=>$product_id,'group_name'=>"Category",'group_relation_id'=>$cat_id));
         $init_obj->library->insert_data('tbl_product_group_relation', array('product_id'=>$product_id,'group_name'=>"Brand",'group_relation_id'=>$brand_id));
      
              foreach($_POST['tag'] as $t)
              {
                $init_obj->library->insert_data('tbl_product_group_relation', array('product_id'=>$product_id,'group_name'=>"Tag",'group_relation_id'=>$t));

              }
              
             
        


        $init_obj->library->set_session( 'message', 'Product Added Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=product-manager' );
    }
} 

else if ( isset( $_POST['cat_id'] ) ) {
    if ( empty( $_POST['cat_id'] ) ) {
        /*print_r( $_POST );
        die();*/
        $init_obj->library->redirect( SITEURL . '/admin' );
    }


    $id = intval( $_POST['cat_id'] );
    //echo $id;
    //die();

    $category_name = $init_obj->library->sanitize_input( $_POST['category_name'] );
    
    $category_description = $init_obj->library->sanitize_input( $_POST['category_description'] );
    $result = $init_obj->library->update_data( 'tbl_category', array( 'category_name' => $category_name, 'category_description' => $category_description ), array( 'cat_id' => $id ) );
    if ( $result ) {
        $init_obj->library->set_session( 'message', 'Category Updated Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=category-manager' );
    }
}
else if ( isset( $_GET['action'], $_GET['cat_id'] ) && $_GET['action'] == 'delete-cat' ) {


    $id = intval( $_GET['cat_id'] );


    $result = $init_obj->library->delete_data( 'tbl_category', array( 'cat_id' => $id ) );

    if ( $result ) {
        //echo "reach";
        //die();
        $init_obj->library->set_session( 'message', 'Category Deleted Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=category-manager' );
    }
} 

else if ( isset( $_POST['brand_id'] ) ) {
    if ( empty( $_POST['brand_id'] ) ) {
        /*print_r( $_POST );
        die();*/
        $init_obj->library->redirect( SITEURL . '/admin' );
    }


    $id = intval( $_POST['brand_id'] );
    //echo $id;
    //die();
    $brand_name = $init_obj->library->sanitize_input( $_POST['brand_name'] );
    
     $brand_description = $init_obj->library->sanitize_input( $_POST['brand_description'] );
      $result = $init_obj->library->update_data( 'tbl_brand', array( 'brand_name' => $brand_name, 'brand_description' => $brand_description ), array( 'brand_id' => $id ) );
    if ( $result ) {
        $init_obj->library->set_session( 'message', 'Brand Updated Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=brand-manager' );
    }
}

else if ( isset( $_GET['action'], $_GET['brand_id'] ) && $_GET['action'] == 'delete-brand' ) {


    $id = intval( $_GET['brand_id'] );


    $result = $init_obj->library->delete_data( 'tbl_brand', array( 'brand_id' => $id ) );

    if ( $result ) {
        //echo "reach";
        //die();
        $init_obj->library->set_session( 'message', 'Brand Deleted Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=brand-manager' );
    }
} 

else if ( isset( $_POST['tag_id'] ) ) {
    if ( empty( $_POST['tag_id'] ) ) {
        /*print_r( $_POST );
        die();*/
        $init_obj->library->redirect( SITEURL . '/admin' );
    }


    $id = intval( $_POST['tag_id'] );
    //echo $id;
    //die();
    $tag_name = $init_obj->library->sanitize_input( $_POST['tag_name'] );
    $tag_description = $init_obj->library->sanitize_input( $_POST['tag_description'] );
    $result = $init_obj->library->update_data( 'tbl_tag', array( 'tag_name' => $tag_name, 'tag_description' => $tag_description ), array( 'tag_id' => $id ) );
    if ( $result ) {
        $init_obj->library->set_session( 'message', 'Tag Updated Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=tag-manager' );
    }
}

else if ( isset( $_GET['action'], $_GET['tag_id'] ) && $_GET['action'] == 'delete-tag' ) {


    $id = intval( $_GET['tag_id'] );


    $result = $init_obj->library->delete_data( 'tbl_tag', array( 'tag_id' => $id ) );

    if ( $result ) {
        //echo "reach";
        //die();
        $init_obj->library->set_session( 'message', 'Tag Deleted Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=tag-manager' );
    }
} 
else if ( isset( $_POST['add_page'] ) ) {
    /*echo "<pre>";
     print_r($_POST);
    print_r($_FILES);
     echo "</pre>";
     die();*/

    $page_title = $init_obj->library->sanitize_input( $_POST['page_title'] );
    $page_slug = $init_obj->library->generate_slug( $page_title, 'tbl_page', 'page_slug' );
    /* echo $page_slug;
      die(); */
    $page_content = $init_obj->library->sanitize_input( $_POST['page_content'] );
    $show = $init_obj->library->sanitize_input( $_POST['show'] );
    $page_order = $init_obj->library->sanitize_input( $_POST['page_order'] );
    $upload_filename = $init_obj->library->page_image();
    $page_template =$init_obj->library->sanitize_input($_POST['page_template']);


    $page_insert = $init_obj->library->insert_data( 'tbl_page', array( 'page_title' => $page_title,
        'page_content' => $page_content,
        'show_in_menu' => $show,
        'page_menu_order' => $page_order,
        'page_image' => $upload_filename,
        'page_slug' => $page_slug,
        'page_templates'=>$page_template ) );
    if ( $page_insert ){
        
            
         $init_obj->library->set_session( 'message', 'Page Added Sucessfully!!' );
    
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=page-manager' );
        }
} 
else if ( isset( $_POST['edit_page'] ) ) {
    //  echo "<pre>";
    //   print_r($_POST);
    //   echo "</pre>";
    //  die();
    // print_r($_FILES);
    // die();
    $page_id = intval( $_POST['page_id'] );

    $previous_image = $init_obj->library->sanitize_input( $_POST['previous_image'] );
    // echo $previous_image;
    // die();
    $previous_slug = $init_obj->library->sanitize_input( $_POST['previous_slug'] );
    // echo $previous_slug;
    //  die();
    $page_title = $init_obj->library->sanitize_input( $_POST['page_title'] );
    $page_content = $init_obj->library->sanitize_input( $_POST['page_content'] );
    $page_order = $init_obj->library->sanitize_input( $_POST['page_order'] );
    $show = $init_obj->library->sanitize_input( $_POST['show'] );
    $page_template = $init_obj->library->sanitize_input( $_POST['page_template'] );

    if ( !empty( $_FILES['page_image']['name'] ) ) {
        $upload_filename = $init_obj->library->page_image();
    }
    if ( isset( $upload_filename ) ) {
        $page_image = $upload_filename;
    } else {
        $page_image = $previous_image;
    }
    $page_slug = $init_obj->library->sanitize_input( $_POST['page_slug'] );
    $page_slug = $init_obj->library->generate_slug( $page_slug, 'tbl_page', 'page_slug' );
    if ( isset( $slug_string ) ) {
        $page_slug = $page_slug;
    } else {
        $page_slug = $previous_slug;
    }
    $page_update = $init_obj->library->update_data( 'tbl_page', array(
        'page_title' => $page_title,
        'page_content' => $page_content,
        'show_in_menu' => $show,
        'page_menu_order' => $page_order,
        'page_image' => $page_image,
        'page_slug' => $page_slug,
        'page_templates' => $page_template ), array( 'page_id' => $page_id ) );
    if ( $page_update ) {
        if ( !empty( $_POST['is_home'] ) ) {
            $init_obj->library->update_data( 'tbl_home', array( 'page_id' => $page_id ), array( 'home_id' => 1 ) );
        }
        $init_obj->library->set_session( 'message', 'Page Detail Updated Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=page-manager' );
    }
}
 else if ( isset( $_GET['action'], $_GET['page_id'] ) && $_GET['action'] == 'delete-page' ) {

    $id = intval( $_GET['page_id'] );
    $result = $init_obj->library->delete_data( 'tbl_page', array( 'page_id' => $id ) );
    if ( $result ) {
        $init_obj->library->set_session( 'message', 'Page Deleted Sucessfully!!' );
        $init_obj->library->redirect( SITEURL . '/admin/index.php?page=page-manager' );
    }
}