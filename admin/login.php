<?php
include('includes/classes/class-init.php');
if(isset($_POST['login_submit']))
{
	 // echo "<pre>";
	 // print_r($_POST);
	 // echo "</pre>";
	 // die();
	$username = $init_obj->library->sanitize_input($_POST['username']);
	$password = $init_obj->library->sanitize_input($_POST['password']);
	$user_row = $init_obj->library->select_data('tbl_users',array(),array('username'=>$username,'password'=>$password));
	  // print_r($user_row);
	  // die();
	if(empty($user_row))
	{

		$_SESSION['error'] = 'Invalid Username or Password';
		$init_obj->library->redirect(SITEURL.'/admin/login-form.php');
	}
	else
	{

		$_SESSION['user_id'] = $user_row[0]['user_id'];
		//echo $_SESSION['user_id'];
		//die();
		$init_obj->library->redirect(SITEURL.'/admin/index.php');
	}
}
else
{
		$init_obj->library->redirect(SITEURL.'/admin/login-form.php');
}