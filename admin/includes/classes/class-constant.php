<?php

class Constant {

    function __construct() {
        $this->define_constants();
    }

    function define_constants() {
        if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '127.0.01') {
            define('SITEURL', 'http://localhost/ecommerce');
            define('HOST', 'localhost');
            define('USER', 'root');
            define('PASS', '');
            define('DBNAME', 'db_ecommerce');
            define('PRODUCT_LIMIT', 4);
        } else {
            define('SITEURL', 'live site url');
            define('HOST', 'live host');
            define('USER', 'live username');
            define('PASS', 'live password');
            define('DBNAME', 'live database name');
            define('PRODUCT_LIMIT', 5);
        }
    }

}
