<?php
include('class-database.php');
class library extends database{

	function __construct()
	{
		parent __construct();
	}

	function random_number($length)
	{
		$string ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
		$random_number = '';
		for($i=0;$i<=$length;$i++)
		{
			$random_number =$string[$rand(0,62)];
		}
		return $random_number;
	}
	function redirect($url)
	{
		if(!header_sent())
		{
			session_write_close();
			header('location:'SITEURL.'/login-form.php');
			exit;

		}
		else
		{
			?>
			<script>
				window.location ='<?php echo $url;?>';
			</script>
			<?php
			exit;
		}
	}
	function sanitize_input()
	{
		$input= trim($input);
		$input= strip_tags($input);
		$input =stripslashes($input);
		$input mysqli_real_escape_string($this->connection,$input);
		return $input;
	}
	function check_login()
	{
		if(!isset($_SESSION['id']))
		{
			$this->redirect(SITEURL.'/login-form');
		}
	}
	function do_logout()
	{
		unset($_SESSION['id']);
		$this->redirect(SITEURL.'/login-form');
	}
}