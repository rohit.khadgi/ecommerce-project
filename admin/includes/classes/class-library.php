

<?php
include('class-database.php');

class library extends database {

    function __construct() {
        parent:: __construct();
    }

    function print_array( $array ) {
        echo "<pre>";
        print_r( $array );
        echo "</pre>";
    }

    function random_number( $length = 10 ) {
        $string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
        $random_string = '';
        for ( $i = 0; $i <= $length; $i++ ) {
            $random_string .= $string[rand( 0, 62 )];
        }
        return $random_string;
    }
      
    function redirect( $url ) {
        if ( !headers_sent() ) {
            session_write_close();
            header( 'location:' . $url );
            exit;
        } else {
            ?>

            <script>
                window.location = '<?php echo $url; ?>';
            </script>
            <?php
            exit;
        }
    }

    function insert_image() {
        if ( !empty( $_FILES['product_image']['name'] ) ) {
            $filename = $_FILES['product_image']['name'];
            $filename_array = explode( '.', $filename );
            $ext = end( $filename_array );
            $random_number = $this->random_number();
            $upload_filename = 'image_' . $random_number . '.' . $ext;
            $source_path = $_FILES['product_image']['tmp_name'];
            $destination_path = '../images/' . $upload_filename;
            @move_uploaded_file( $source_path, $destination_path );
        } else {
            $upload_filename = '';
        }
        return $upload_filename;
    }

    function slider_image() {
        if ( !empty( $_FILES['slider_image']['name'] ) ) {
            $filename = $_FILES['slider_image']['name'];
            $filename_array = explode( '.', $filename );
            $ext = end( $filename_array );
            $random_number = $this->random_number();
            $upload_filename = 'image_' . $random_number . '.' . $ext;
            $source_path = $_FILES['slider_image']['tmp_name'];
            $destination_path = '../images/' . $upload_filename;
            @move_uploaded_file( $source_path, $destination_path );
        } else {
            $upload_filename = '';
        }
        return $upload_filename;
    }

    function page_image() {
        if ( !empty( $_FILES['page_image']['name'] ) ) {
            $filename = $_FILES['page_image']['name'];
            $filename_array = explode( '.', $filename );
            $ext = end( $filename_array );
            $random_number = $this->random_number();
            $upload_filename = 'image_' . $random_number . '.' . $ext;
            $source_path = $_FILES['page_image']['tmp_name'];
            $destination_path = '../images/' . $upload_filename;
            @move_uploaded_file( $source_path, $destination_path );
        } else {
            $upload_filename = '';
        }
        return $upload_filename;
    }
     function ad_image() {
        if ( !empty( $_FILES['ad_image']['name'] ) ) {
            $filename = $_FILES['ad_image']['name'];
            $filename_array = explode( '.', $filename );
            $ext = end( $filename_array );
            $random_number = $this->random_number();
            $upload_filename = 'image_' . $random_number . '.' . $ext;
            $source_path = $_FILES['ad_image']['tmp_name'];
            $destination_path = '../images/' . $upload_filename;
            @move_uploaded_file( $source_path, $destination_path );
        } else {
            $upload_filename = '';
        }
        return $upload_filename;
    }

    function sanitize_input( $input ) {
        $input = trim( $input );
        $input = strip_tags( $input );
        $input = stripslashes( $input );
        $input = mysqli_real_escape_string( $this->connection, $input );
        
        return $input;
    }

    function check_login() {
        if ( !isset( $_SESSION['user_id'] ) ) {

            $this->redirect( SITEURL . '/admin/login-form.php' );
        }
    }

    function do_logout() {
        unset( $_SESSION['user_id'] );
        $this->redirect( SITEURL . '/admin/login-form.php' );
    }

    function set_session( $session_key, $session_value ) {
        $_SESSION[$session_key] = $session_value;
    }

    function print_session( $session_key ) {
        if ( isset( $_SESSION[$session_key] ) ) {
            echo $_SESSION[$session_key];
            unset( $_SESSION[$session_key] );
        }
    }

    function slugify( $string ) {
        $string = utf8_encode( $string );
        $string = iconv( 'UTF-8', 'ASCII//TRANSLIT', $string );
        $string = preg_replace( '/[^a-z0-9- ]/i', '', $string );
        $string = str_replace( ' ', '-', $string );
        $string = trim( $string, '-' );
        $string = strtolower( $string );

        if ( empty( $string ) ) {
            return 'n-a';
        }

        return $string;
    }

    function generate_slug( $title, $table_name, $slug_field ) {
        $slug_string = $this->slugify( $title );
        $slug_check_flag = $this->check_if_slug_exists( $slug_string, $table_name, $slug_field );
        $slug_count = 0;
        while ( $slug_check_flag ) {
            $slug_count++;
            $new_slug = $slug_string . '-' . $slug_count;
            $slug_check_flag = $this->check_if_slug_exists( $new_slug, $table_name, $slug_field );
        }
        if ( isset( $new_slug ) ) {
            $slug_string = $new_slug;
        }
        return $slug_string;
    }

    function check_if_slug_exists( $slug_string, $table_name, $slug_field ) {
        $slug_rows = $this->select_data( $table_name, array(), array( $slug_field => $slug_string ) );
        if ( empty( $slug_rows ) ) {
            return false;
        } else {
            return true;
        }
    }

    function get_home_page_id() {
        $home_row = $this->select_data( 'tbl_home', array(), array( 'home_id' => 1 ) );

        $home_page_id = $home_row[0]['page_id'];
        return $home_page_id;
    }

    function get_home_page_slug() {
        $home_page_id = $this->get_home_page_id();
        $home_page_row = $this->select_data( 'tbl_pages', array( 'page_slug' ), array( 'page_id' => $home_page_id ) );
        return $home_page_row[0]['page_slug'];
    }

    function get_product_by_category( $product_category_slug, $current_page = 1 ) {
        $product_category_id = $this->get_product_category_id( $product_category_slug );
        if ( !empty( $product_category_id ) ) {
            $limit = PRODUCT_LIMIT;
            $offset = ($current_page - 1) * $limit;
            $category_product = $this->select_data( 'tbl_product', array(), array( 'cat_id' => $product_category_id ), 'and', 'cat_id', 'desc', $offset, $limit );
            return $category_product;

        } else {
            return NULL;
        }
    }

    function get_product_category_id( $product_category_slug ) {
        $news_category_rows = $this->select_data( 'tbl_category', array( 'cat_id' ), array( 'category_slug' =>$product_category_slug) );
        if ( !empty( $news_category_rows ) ) {
            return $news_category_rows[0]['cat_id'];
        } else {
            return NULL;
        }
    }
       function get_product_by_brand( $product_brand_slug, $current_page = 1 ) {
        $product_brand_id = $this->get_product_brand_id( $product_brand_slug );
        if ( !empty( $product_brand_id ) ) {
            $limit = PRODUCT_LIMIT;
            $offset = ($current_page - 1) * $limit;
            $brand_product = $this->select_data( 'tbl_product', array(), array( 'brand_id' => $product_brand_id ), 'and', 'brand_id', 'desc', $offset, $limit );
            return $brand_product;
        } else {
            return NULL;
        }
    }
     function get_product_brand_id( $product_brand_slug ) {
        $product_brand_rows = $this->select_data( 'tbl_brand', array( 'brand_id' ), array( 'brand_slug' =>$product_brand_slug) );
        if ( !empty( $product_brand_rows ) ) {
            return $product_brand_rows[0]['brand_id'];
        } else {
            return NULL;
        }
    }

 function get_product_by_brand_with_category( $product_brand_slug, $product_category_slug, $current_page = 1 ) {
        $product_category_id = $this->get_product_category_id($product_category_slug);
        $product_brand_id = $this->get_product_brand_id( $product_brand_slug );
        if ( !empty( $product_brand_id ) ) {
            $limit = PRODUCT_LIMIT;
            $offset = ($current_page - 1) * $limit;
            $brand_product = $this->select_data( 'tbl_product', array(), array( 'brand_id' => $product_brand_id ,'cat_id'=> $product_category_id), 'and', 'brand_id', 'desc', $offset, $limit );
            return $brand_product;
        } else {
            return NULL;
        }
    }
    function get_category_total_row_count( $news_category_slug ) {
        $news_category_id = $this->get_news_category_id( $news_category_slug );
        $count_query = "select count(*) from tbl_news where cat_id = $news_category_id";
        $count_result = $this->exec_query( $count_query );
        $count_row = $this->fetch_assoc( $count_result );
        $total_row_count = $count_row['count(*)'];
        $total_page = $total_row_count / NEWS_LIMIT;
        if ( $total_row_count % NEWS_LIMIT != 0 ) {
            $total_page = intval( $total_page ) + 1;
        }
        return $total_row_count;
    }

    function get_news_by_keyword( $search_keyword, $current_page = 1 ) {
        $limit = NEWS_LIMIT;
        $offset = ($current_page - 1) * $limit;
        $search_query = "select * from tbl_news where news_title like '%$search_keyword%' or news_description like '%$search_keyword%' order by news_id desc limit $offset,$limit";
        $search_result = $this->exec_query( $search_query );
        $news = array();
        if ( $this->num_rows( $search_result ) ) {
            while ( $news_row = $this->fetch_assoc( $search_result ) ) {
                $news[] = $news_row;
            }
        }

        return $news;
    }

    function get_search_total_row_count( $search_keyword ) {
        $count_query = "select count(*) from tbl_news where news_title like '%$search_keyword%' or news_description like '%$search_keyword%' order by news_id desc";
        $count_result = $this->exec_query( $count_query );
        $count_row = $this->fetch_assoc( $count_result );
        $total_row_count = $count_row['count(*)'];
        $total_page = $total_row_count / NEWS_LIMIT;
        if ( $total_row_count % NEWS_LIMIT != 0 ) {
            $total_page = intval( $total_page ) + 1;
        }
        return $total_row_count;
    }

}
