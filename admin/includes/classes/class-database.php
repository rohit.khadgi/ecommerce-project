<?php

include('class-constant.php');

class database extends Constant {

    var $connection;

    // var $tbl_name;
    // var $columns;
    // var $order;
    // var $order_by;
    // var $where_condition;
    // var $where_condition_join;
    function __construct() {
        parent:: __construct();
        $this->connect_host();
    }

    function connect_host() {
        $this->connection = mysqli_connect( HOST, USER, PASS, DBNAME ) or die(mysqli_connect_error());
    }
  

    function insert_id(){
        return mysqli_insert_id($this->connection);
    }

    function exec_query( $query ) {
        if ( isset( $_GET['query_debug'] ) ) {
            echo $query;
            die();
        }
        $result = mysqli_query( $this->connection, $query ) or die(mysqli_error($this->connection));
        return $result;
    }

    function num_rows( $result ) {
        return mysqli_num_rows( $result );
    }

    function fetch_assoc( $result ) {
        return mysqli_fetch_assoc( $result );
    }

    function insert_data( $table_name, $insert_data ) {
        $insert_query = "insert into $table_name set ";
        //echo "<pre>";
        //print_r($insert_data);
        //echo "</pre>";
        //die();

        if ( !empty( $insert_data ) ) {
            $insert_array = array();
            foreach ( $insert_data as $key => $val ) {
                if ( is_string( '$val' ) ) {
                    $insert_array[] = "$key = '$val'";
                } else {
                    $insert_array[] = "$key =$val";
                }
            }
        }
        //echo "<pre>";
        //print_r($insert_array);
        //echo "<?pre>";
        //die();

        $insert_data_string = implode( ',', $insert_array );
        //echo $insert_data_string;
        //die();
        $insert_query .= $insert_data_string;
        //echo $insert_query;
        //die();
        $insert_result = $this->exec_query( $insert_query );
        return $insert_result;
    }

    function delete_data( $tbl_name, $delete_condition = '', $delete_condition_join = 'or' ) {
        $delete_query = "delete from $tbl_name";
        if ( !empty( $delete_condition ) ) {
            $delete_query .= ' where ';
            // echo $delete_query;
            //        echo "<pre>";
            //         print_r($delete_condition);
            //      echo "</pre>";
            //     die();
            $delete_condition_array = array();
            foreach ( $delete_condition as $key => $val ) {
                if ( is_string( $val ) ) {
                    $delete_condition_array[] = "$key ='$val'";
                } else {
                    $delete_condition_array[] = "$key = $val";
                }
                //  echo "<pre>";
                //  print_r($delete_condition_array);
                //  echo "</pre>";
                // die();
            }

            $delete_condition_string = implode( ' ' . $delete_condition_join . ' ', $delete_condition_array );
            // echo "<pre>";
            // print_r($delete_condition_array);
            //        		echo $delete_condition_string;
            //         	echo "</pre>";
            //die();
            //echo "<Br/>";
            //die();

            $delete_query .= $delete_condition_string;
            // echo $delete_query;
            // die();
            $delete_result = $this->exec_query( $delete_query );
            return $delete_result;
        }
    }

    function update_data( $tbl_name, $update_data, $where_condition = array(), $where_condition_join = 'and' ) {
        if ( !empty( $update_data ) )
            $update_query = "update $tbl_name set "; {
            //print_r($where_condition);
            //die();
            $update_array = array();
            foreach ( $update_data as $key => $val ) {
                if ( is_string( $val ) ) {
                    $update_array[] = "$key ='$val'";
                } else {
                    $update_array[] = "$key = $val";
                }
            }
            //print_r($update_array);
            //die();

            $update_string = implode( ',', $update_array );
            //echo $update_string;
            //die();
            $update_query .= $update_string;
            //echo $update_query;
            //die();
            if ( !empty( $where_condition ) ) {
                $where_array = array();
                $update_query .= ' where ';
                foreach ( $where_condition as $k => $v ) {
                    if ( is_string( $v ) ) {
                        $where_array[] = "$k='$v'";
                    } else {
                        $where_array[] = "$k = $v";
                    }
                }
                $where_string = implode( ' ' . $where_condition_join . ' ', $where_array );
                $update_query .= $where_string;
                //echo $update_query;
                //die();
                $update_result = $this->exec_query( $update_query );
                return $update_result;
            }
        }
    }

    function select_data( $table_name, $columns = array(), $where_condition = array(), $where_condition_join = 'and', $order_by = '', $order = 'asc', $o = '', $l = '' ) {

        $columns = (!empty( $columns )) ? implode( ',', $columns ) : '*';
        $select_query = "select $columns from $table_name";
   /*     echo $select_query;
        die();*/
        if ( !empty( $where_condition ) ) {
            $select_query .= ' where ';
            $where_condition_array = array();
            foreach ( $where_condition as $k => $v ) {
                if ( is_string( $v ) ) {
                    $where_condition_array[] = "$k = '$v'";
                } else {
                    $where_condition_array[] = "$k = $v";
                }
            }
            $where_condition_string = implode( ' ' . $where_condition_join . ' ', $where_condition_array );

            $select_query .= $where_condition_string;
           /* echo $select_query;
            die();*/
        }
        if ( !empty( $order_by ) && !empty( $order ) ) {
            $select_query .= ' order by ' . $order_by . ' ' . $order;
            // echo $select_query;
            // die();
        }

        if ( !empty( $l ) ) {

            $select_query .= ' limit ' . $o . ',' . $l;
        }
        $select_result = $this->exec_query( $select_query );
        $select_data = array();
        if ( $this->num_rows( $select_result ) > 0 ) {
            while ( $select_row = $this->fetch_assoc( $select_result ) ) {
                $select_data[] = $select_row;
            }
        }
      /*  print_r($select_data);
        die();*/

        return $select_data;
    }

    // echo $select_query;
    // die();
    function select_search( $table_name, $search_keyword = '', $order_by = '', $order = 'desc' ) {

        $select_query = "select * from $table_name where news_title like '%$search_keyword%' or news_description like '%$search_keyword%'";
        //echo $select_query;
        //die();

        if ( !empty( $order_by ) && !empty( $order ) ) {
            $select_query .= ' order by ' . $order_by . ' ' . $order;
            // echo $select_query;
            // die();
        }
        $select_result = $this->exec_query( $select_query );
        $select_data = array();
        if ( $this->num_rows( $select_result ) > 0 ) {
            while ( $select_row = $this->fetch_assoc( $select_result ) ) {
                $select_data[] = $select_row;
            }
        }
        //print_r($select_data);
        //die();
        print_r( $select_data );
        die();
        return $select_data;
    }

    function select_join( $tbl1, $tbl2, $col = array(), $where_condition = array(), $order_by = '', $order = 'asc', $where_condition_join = 'and' ) {
        $col = (!empty( $col )) ? implode( ',', $col ) : '*';
        $select_query = "select $col from $tbl1 inner join $tbl2 on ";
        $select_query .= $tbl1 . '.cat_id' . ' = ' . $tbl2 . '.cat_id';
        // echo $select_query;
        // die();
        if ( !empty( $where_condition ) ) {
            $select_query .= ' where ';
            // echo $select_query;
            // die();
            $where_condition_array = array();
            foreach ( $where_condition as $i => $j ) {
                if ( is_string( $j ) ) {
                    $where_condition_array[] = "$i = '$j'";
                } else {
                    $where_condition_array[] = "$i = $j";
                }
            }
            $where_condition_string = implode( ' ' . $where_condition_join . ' ', $where_condition_array );
            $select_query .= $where_condition_string;
        }
        //echo $select_query;
        //die();
        if ( !empty( $order_by ) && !empty( $order ) ) {
            $select_query .= ' order by ' . $order_by . ' ' . $order;
          /*  echo $select_query;
            die();*/
        }
        $select_result = $this->exec_query( $select_query );
        $select_data = array();
        if ( $this->num_rows( $select_result ) > 0 ) {
            while ( $select_row = $this->fetch_assoc( $select_result ) ) {
                $select_data[] = $select_row;
            }
        }
        return $select_data;
        //echo $select_query;
        //die();
    }

    // function select_new_data($tbl_name)
    // {
    // 	$columns = (!empty($this->columns)) ? implode(',',$this->columns):'*';
    // 	//var_dump($columns);die();
    // 	//$select_query = "select $columns from $tbl_name";
    // 	//echo $select_query;
    // 	die();
    // 	 if (!empty($this->where_condition)) {
    //          $select_query .= ' where ';
    //          //echo $select_query;
    //         //die();
    //          $where_condition_array = array();
    //          foreach($this->where_condition as $k => $v) {
    //              if (is_string($v)) {
    //                  $where_condition_array[] = "$k = '$v'";
    //              } else {
    //                  $where_condition_array[] = "$k = $v";
    //              }
    //          }
    //          $where_condition_join = (!empty($this->where_condition_join)) ? $this->where_condition_join : 'and';
    //          $where_condition_string = implode(' ' . $where_condition_join . ' ', $where_condition_array);
    //          $select_query .= $where_condition_string;
    //      }
    //          //echo $select_query;
    //          //die();
    //          //echo $this->order_by;
    //      	//die();
    //          if(!empty($this->order_by)){
    //          	$order = (!empty($this->order))? $this->order : 'asc';
    //          	$select_query .= ' order by '.$this->order_by. ' '.$order;
    //          	//echo $select_query ;
    //          	//die();
    //          }
    //          $select_result = $this->exec_query($select_query);
    //          $data = array();
    //          if($this->num_rows($select_result)>0)
    //          {
    //          	while($select_row = $this->fetch_assoc($select_result))
    //          	{
    //          		$data[] = $select_row;
    //          	}
    //          }
    //          return $data;
    // }
    // function reset_query_parameters() {
    //     unset($this->tbl_name);
    //     unset($this->columns);
    //     unset($this->order);
    //     unset($this->order_by);
    //     unset($this->where_condition);
    //     unset($this->where_condition_join);
    // }
}
