<h2 class="site-title">User Manager</h2>
<?php 
	if(isset($_GET['action']))
	{
		
		$action = $init_obj->library->sanitize_input($_GET['action']);
		$action_page = $action.'.php';
		if(file_exists('includes/views/manager/user-manager/'.$action_page))
		{
			
			include('includes/views/manager/user-manager/'.$action_page);
		}
		else
		{
			include('includes/views/manager/user-manager/list-table.php');
		}
	}
	else
	{
		include('includes/views/manager/user-manager/list-table.php');
	}
 ?>