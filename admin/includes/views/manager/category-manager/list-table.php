
		<h2 class="site-title">Category List</h2>
		<table class="manager-list-table">
			<tr>
				<td colspan="12">
				  <?php $init_obj->library->print_session('message'); ?>
					<a href="<?php echo SITEURL. '/admin/index.php?page=category-manager&action=add-category';?>"><input type="button" name="add-category" class="button-secondary float-right" value="Add category"></a>
					

				</td>
			</tr>
			<tr>
				<td>SN</td>
	
	
			<td>
					Category Name
			</td>
			<td>
				Category Description
			</td>
			<td>
				Category Slug
			</td>
			
			<td>
				Action
			</td>
			</tr>
			 <?php
    $users = $init_obj->library->select_data('tbl_category');

    if(!empty($users))
    {
        $sn=1;
        foreach($users as $user)
        {
               // print_r($user);
               // die();
            ?>
            <tr>
            <td><?php echo $sn++;?></td>
            <td><?php echo $user['category_name'];?></td>
            <td><?php echo $user['category_description'];?></td>
            <td><?php echo $user['category_slug'];?></td>
            
            <td>
                <a href="<?php echo SITEURL.'/admin/index.php?page=category-manager&action=edit-cat&cat_id='.$user['cat_id'];?>">Edit</a> &nbsp;|&nbsp;
                <a href="<?php echo SITEURL.'/admin/action.php?action=delete-cat&cat_id='.$user['cat_id'];?>" onclick="return confirm('Are you sure you want to delete?');">Delete</a>

            </td>
        </tr>
        <?php
      }  
    }
    ?>
		
</table>
