<?php
if(empty($_GET['cat_id']))
{
    $init_obj->library->redirect(SITEURL.'/admin');
}
$id = intval($_GET['cat_id']);
$user_result = $init_obj->library->select_data('tbl_category',array(),array('cat_id'=>$id));
//print_r($user_result);
//die();
if(empty($_GET['cat_id']))
{
    $int_obj->library->redirect(SITEURL.'/admin');
}
$user = $user_result[0];
//print_r($user);
//die();
?>


<h3 class="center">Edit Category</h3>
<form method="post" action="<?php echo SITEURL . '/admin/action.php'; ?>" class="center" style="width:30%;margin: 50px auto;">
    <div class="field-wrap">
        <label>Category Name</label>
        <div class="field"><input type="text" name="category_name" value="<?php echo $user['category_name'];?>" /></div>
    </div>
    <div class="field-wrap">
        <label>Category Description</label>
        <div class="field"><textarea rows="20" cols="50" name="category_description"><?php echo $user['category_description'];?></textarea></div>
    </div>
    <div class="field-wrap">
        <label></label>
        <div class="field">
            <input type="submit" name="edit_cat" value=Edit Category" class="button-primary"/>
        </div>
    </div>
    <input type="hidden" name="cat_id" value="<?php echo $user['cat_id']; ?>"/>
</form>
