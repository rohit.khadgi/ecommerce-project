<h1 class="site-title">Add Page </h1>

	<form method="post" action="<?php echo SITEURL.'/admin/action.php';?>" enctype="multipart/form-data">
	<table class="manager-list-table">
			
			<tr>
				<td>
					<strong>Page Title</strong>
				</td>
				<td>
					<input type="text" name="page_title" required="">
				</td>
			</tr>
				<tr>
					<td>
						<strong>Page Content</strong>
					</td>
					<td>
						<textarea rows="20" cols="50" name="page_content" required=""></textarea>
					</td>
				</tr>
				
				<tr>
					<td>Page Image</td>
					<td><input type="file" name="page_image" ></td>
				</tr>
				<tr>
				<td>
					<strong>Page Menu Order</strong>
				</td>
				<td>
					<input type="text" name="page_order" required="">
				</td>
			</tr>
				<tr>
					<td>
					<strong>	Show in Menu</strong>
					</td>
					<td>
						 <input type="radio" name="show" value="yes" checked> Yes<br>
  							<input type="radio" name="show" value="no">No<br>
					</td>
				</tr>
				
        <tr>
            <td>
                <strong>Page Template</strong>
            </td>
            <td>
                <select name="page_template">
                    <option value="default" >Default Template</option>
                    <option value="product-category" >Product Category Template</option>
                    <option value="product-brand" >Product Brand Template</option>
                     <option value="product-cart" >Cart Template</option>
                     <option value="check-out" >Check-out Template</option>vs
                </select>
            </td>
        </tr>
				
				<tr>
					<td></td>
					<td>
						<input type="submit" name="add_page" value="Add page">
					</td>
				</tr>



			
		</table>
	</form>
