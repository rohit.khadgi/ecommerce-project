
<h2 class="site-title">Page List</h2>
<table class="manager-list-table">
    <tr>
        <td colspan="9">
            <?php $init_obj->library->print_session( 'message' ); ?>
            <a href="<?php echo SITEURL . '/admin/index.php?page=page-manager&action=add-page'; ?>"><input type="button" name="add-page" class="button-secondary float-right" value="Add Page"></a>


        </td>
    </tr>
    <tr>
        <td>SN</td>


        <td>
            Page Title
        </td>
        <td>
            Page Description
        </td>
        <td>
            Page Image
        </td>
        <td>
            Page Template
        </td>
        <td>
            Page Menu Order
        </td>
        <td>Show in Menu</td>
        <td>Page Slug</td>
        <td>
            Action
        </td>
    </tr>
    <?php
    $page = $init_obj->library->select_data( 'tbl_page' );
    // print_r($page);
    // die();
    if ( !empty( $page ) ) {
        $sn = 1;
        foreach ( $page as $user ) {
            ?>
            <tr>
                <td><?php echo $sn++; ?></td>
                <td><?php echo $user['page_title']; ?></td>
                <td><?php echo $user['page_content']; ?></td>
                <td><?php
                    if ( !empty( $user['page_image'] ) ) {
                        $page_image = $user['page_image'];
                        ?>
                        <img src="../images/<?php echo $page_image; ?>" width="100px">
                        <?php
                    }
                    ?></td>
                <td><?php echo $user['page_templates']; ?></td>

                <td><?php echo $user['page_menu_order']; ?></td>
                <td><?php echo $user['show_in_menu']; ?></td>
                <td><?php echo $user['page_slug']; ?></td>
                <td>
                    <a href="<?php echo SITEURL . '/admin/index.php?page=page-manager&action=edit-page&page_id=' . $user['page_id']; ?>">Edit</a> &nbsp;|&nbsp;
                    <a href="<?php echo SITEURL . '/admin/action.php?action=delete-page&page_id=' . $user['page_id']; ?>" onclick="return confirm('Are you sure you want to delete?');">Delete</a>

                </td>
            </tr>
            <?php
        }
    }
    ?>
</table>
