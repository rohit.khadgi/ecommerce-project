<?php
if ( empty( $_GET['page_id'] ) ) {
    $init_obj->library->redirect( SITEURL . '/admin' );
}
$page_id = intval( $_GET['page_id'] );
// echo "$id";
$page_result = $init_obj->library->select_data( 'tbl_page', array(), array( 'page_id' => $page_id ) );
// echo "<pre>";
// print_r($page_result);
// echo "</pre>";
// die();

$page = $page_result[0];
//print_r($page);
//die();
$home_page_id = $init_obj->library->get_home_page_id();
?>
<h1 class="site-title">Edit Page </h1>

<form method="post" action="<?php echo SITEURL . '/admin/action.php'; ?>" enctype="multipart/form-data">
    <table class="manager-list-table">

        <tr>
            <td>
                <strong>Page Title</strong>
            </td>
            <td>
                <input type="text" name="page_title" required="" value="<?php echo $page['page_title']; ?>">
            </td>
        </tr>
        <tr>
            <td>
                <strong>Page Content</strong>
            </td>
            <td>
                <textarea rows="20" cols="50" name="page_content"><?php echo $page['page_content']; ?></textarea>
            </td>
        </tr>

        <tr>
            <td>Page Image</td>
            <td><input type="file" name="page_image">
                <input type="hidden" name="previous_image" value="<?php echo $page['page_image']; ?>">
                <?php
                if ( !empty( $page['page_image'] ) ) {
                    $page_image = $page['page_image'];
                    ?>
                    <img src="../images/<?php echo $page_image; ?>" width="100px">
                    <?php
                }
                ?>
        </tr>
        <tr>
            <td>
                <strong>Page Menu Order</strong>
            </td>
            <td>
                <input type="text" name="page_order" required="" value="<?php echo $page['page_menu_order']; ?>">
            </td>
        </tr>
        <tr>
            <td>
                <strong>	Show in Menu</strong>
            </td>
            <td>
                <input type="radio" name="show" value="yes" <?php echo ($page['show_in_menu'] == 'yes') ? 'checked="checked"' : ''; ?>>Yes<br>
                <input type="radio" name="show" value="no"<?php echo ($page['show_in_menu'] == 'no') ? 'checked="checked"' : ''; ?>>No<br>
            </td>
        </tr>

        <tr>
            <td>
                <strong>Is Home?</strong>
            </td>
            <td>
                <label><input type="radio" name="is_home" value="1" <?php echo ($page['page_id'] == $home_page_id) ? 'checked="checked"' : ''; ?>>Yes</label>
                <label><input type="radio" name="is_home" value="0"<?php echo ($page['page_id'] != $home_page_id) ? 'checked="checked"' : ''; ?>>No</label>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Page Template</strong>
            </td>
            <td>
                <select name="page_template">
                    <option value="default" <?php echo ($page['page_templates'] == 'default') ? 'selected="selected"' : ''; ?>>Default Template</option>
                    <option value="product-category" <?php echo ($page['page_templates'] == 'product-category') ? 'selected="selected"' : ''; ?>>Product Category Template</option>
                    <option value="product-brand" <?php echo ($page['page_templates'] == 'product-brand') ? 'selected="selected"' : ''; ?>>Product Brand Template</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>Page Slug</strong></td>
            <td>
                <input type="text" name="page_slug" value="<?php echo $page['page_slug']; ?>">
                <input type="hidden" name="previous_slug" value="<?php echo $page['page_slug']; ?>">
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="submit" name="edit_page" value="Edit page">
            </td>
        </tr>
    </table>
    <input type="hidden" name="page_id" value="<?php echo $page['page_id']; ?>">
</form>
