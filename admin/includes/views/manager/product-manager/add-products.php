<h1 class="site-title">Add Product </h1>

<form method="post" action="<?php echo SITEURL . '/admin/action.php'; ?>" enctype="multipart/form-data">
    <table class="manager-list-table">

        <tr>
            <td>
                <strong>Product Name</strong>
            </td>
            <td>
                <input type="text" name="product_name" required="">
            </td>
        </tr>
        <tr>
            <td>
                <strong>Produt Description</strong>
            </td>
            <td>
                <textarea rows="20" cols="50" name="product_description" id="ckeditor"></textarea>
            </td>
        </tr>
       
        </tr>
           <tr>
            <td>
                <strong>Product Price</strong>
            </td>
            <td>
                <input type="text" name="product_price" required="">
            </td>
        </tr>
        <tr>
            <td>
                <strong>Product Category</strong>
            </td>
            <td>
                <select name="cat_id">
                    <option value="">Choose Category</option>
                    <?php
                    $category = $init_obj->library->select_data( 'tbl_category', array(), array() );
                    //echo "<pre>";
                    //print_r($category);
                    // echo "</pre>";
                    //die();
                    end($category );
                    $key = key( $category );


                    for ( $i = 0; $i <= $key; $i++ ) {
                        ?>
                        <option value="<?php echo $category[$i]['cat_id']; ?>"><?php echo $category[$i]['category_name']; ?></option>
                        <?php
                    }
                    ?>

                </select>
            </td>
        </tr>
           <tr>
            <td>
                <strong>Product Brand</strong>
            </td>
            <td>
                <select name="brand_id">
                    <option value="">Choose Category</option>
                    <?php
                    $brand = $init_obj->library->select_data( 'tbl_brand', array(), array() );
                    //echo "<pre>";
                    //print_r($category);
                    // echo "</pre>";
                    //die();
                    end($brand );
                    $key = key( $brand );


                    for ( $i = 0; $i <= $key; $i++ ) {
                        ?>
                        <option value="<?php echo $brand[$i]['brand_id']; ?>"><?php echo $brand[$i]['brand_name']; ?></option>
                        <?php
                    }
                    ?>

                </select>
            </td>
        </tr>
           <tr>
            <td>
                <strong>Product Tags</strong>
            </td>
            <td>
             <?php
                    $tag = $init_obj->library->select_data( 'tbl_tag', array(), array() );
                    //echo "<pre>";
                    //print_r($category);
                    // echo "</pre>";
                    //die();
                    end($tag );
                    $key = key( $tag );


                    for ( $i = 0; $i <= $key; $i++ ) {
                        ?>
              <input type="checkbox" name="tag[]" value="<?php echo $tag[$i]['tag_id'] ?>"><?php echo $tag[$i]['tag_name'];?>
              <?php 
          }
          ?>
      </td>
        </tr>
        <tr>
            <td>Product Image</td>
            <td><input type="file" name="product_image"></td>
        </tr>
         <tr>
            <td>Product Gallery</td>
            <td>
                <div class="product-gallery-field">
                <input type="file" name="file_img[]"/>

                </div>
                <input type="button" value="Add More" id="gallery-add-trigger">

            </td>
        </tr>
       
        <tr>

            <td>

            </td>
            <td>
                <input type="submit" name="add_product" value="Add Product">
            </td>
        </tr>




    </table>
</form>
