<?php
if ( empty( $_GET['news_id'] ) ) {
    $init_obj->library->redirect( SITEURL . '/admin' );
}
$news_id = intval( $_GET['news_id'] );
// echo "$id";
// die();
$user_result = $init_obj->library->select_join( 'tbl_news', 'tbl_news_category', array(), array( 'news_id' => $news_id ), 'news_id', 'desc', '' );
// echo "<pre>";
// print_r($user_result);
// echo "</pre>";
// die();

$user = $user_result[0];
//print_r($user);
//die();
?>
<h1 class="site-title">Edit News </h1>


<form method="post" action="<?php echo SITEURL . '/admin/action.php'; ?>" enctype="multipart/form-data">
    <table class="manager-list-table">

        <tr>
            <td>
                <strong>News Title</strong>
            </td>
            <td>
                <input type="text" name="news_title" required="" value="<?php echo $user['news_title']; ?>">
            </td>
        </tr>
        <tr>
            <td>
                <strong>News Description</strong>
            </td>
            <td>
                <textarea rows="20" cols="50" name="news_description" id="ckeditor"><?php echo $user['news_description']; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <strong>News Date</strong>
            </td>
            <td>
                <input type="Date" name="news_date" value="<?php echo $user['news_date']; ?>">
            </td>
        </tr>
        <tr>
            <td>
                <strong>News Category</strong>
            </td>
            <td>
                <select name="cat_id">
                    <option value="">Choose Category</option>
                    <?php
                    $category = $init_obj->library->select_data( 'tbl_news_category', array(), array() );
//echo "<pre>";
//print_r($category);
// echo "</pre>";
//die();
                    end( $category );
                    $key = key( $category );


                    for ( $i = 0; $i <= $key; $i++ ) {
                        ?>
                        <option value="<?php echo $category[$i]['cat_id']; ?>"<?php
                        if ( $category[$i]['cat_id'] == $user['cat_id'] ) {
                            echo 'selected="selected"';
                        }
                        ?>><?php echo $category[$i]['category_name']; ?></option>
                                <?php
                            }
                            ?>

                </select>
            </td>
        </tr>
        <tr>
            <td>News Image</td>
            <td><input type="file" name="news_image">
                <input type="hidden" name="previous_image" value="<?php echo $user['news_image']; ?>">
                <?php
                if ( !empty( $user['news_image'] ) ) {
                    $news_image = $user['news_image'];
                    ?>
                    <img src="../images/<?php echo $news_image; ?>" width="100px">
                    <?php
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>
                News Slug
            </td>
            <td>
                <input type="text" name="news_slug" value="<?php echo $user['news_slug']; ?>">
                <input type="hidden" name="previous_news_slug" value="<?php echo $user['news_slug']; ?>">
            </td>
        </tr>
        <tr>
            <td>
                <strong>	Featured News</strong>
            </td>
            <td>
                <input type="radio" name="show" value="yes" <?php echo ($user['featured_post'] == 'yes') ? 'checked="checked"' : ''; ?>>Yes<br>
                <input type="radio" name="show" value="no"<?php echo ($user['featured_post'] == 'no') ? 'checked="checked"' : ''; ?>>No<br>
            </td>
        </tr>
        <tr>
            <td><strong>Editor POST</strong></td>
            <td>

                <input type="radio" name="eshow" value="yes" <?php echo ($user['editor_post'] == 'yes') ? 'checked="checked"' : ''; ?>>Yes<br>
                <input type="radio" name="eshow" value="no"<?php echo ($user['editor_post'] == 'no') ? 'checked="checked"' : ''; ?>>No<br>

            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>
                <input type="submit" name="edit_news" value="Edit News">
            </td>
        </tr>




    </table>
    <input type="hidden" name="news_id" value="<?php echo $news_id; ?>">
</form>
