<?php
if(empty($_GET['brand_id']))
{
    $init_obj->library->redirect(SITEURL.'/admin');
}
$id = intval($_GET['brand_id']);
$user_result = $init_obj->library->select_data('tbl_brand',array(),array('brand_id'=>$id));
//print_r($user_result);
//die();
if(empty($_GET['brand_id']))
{
    $int_obj->library->redirect(SITEURL.'/admin');
}
$user = $user_result[0];
//print_r($user);
//die();
?>


<h3 class="center">Edit Brand</h3>
<form method="post" action="<?php echo SITEURL . '/admin/action.php'; ?>" class="center" style="width:30%;margin: 50px auto;">
    <div class="field-wrap">
        <label>Brand Name</label>
        <div class="field"><input type="text" name="brand_name" value="<?php echo $user['brand_name'];?>" /></div>
    </div>
    <div class="field-wrap">
        <label>Brand Description</label>
        <div class="field"><textarea rows="20" cols="50" name="brand_description"><?php echo $user['brand_description'];?></textarea></div>
    </div>
    <div class="field-wrap">
        <label></label>
        <div class="field">
            <input type="submit" name="edit_brand" value=Edit Brand" class="button-primary"/>
        </div>
    </div>
    <input type="hidden" name="brand_id" value="<?php echo $user['brand_id']; ?>"/>
</form>
