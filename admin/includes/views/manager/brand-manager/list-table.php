
		<h2 class="site-title">Brand List</h2>
		<table class="manager-list-table">
			<tr>
				<td colspan="11">
				  <?php $init_obj->library->print_session('message'); ?>
					<a href="<?php echo SITEURL. '/admin/index.php?page=brand-manager&action=add-brand';?>"><input type="button" name="add-brand" class="button-secondary float-right" value="Add brand"></a>
					

				</td>
			</tr>
			<tr>
				<td>SN</td>
	
	
			<td>
					Brand Name
			</td>
			<td>
				Brand Description
			</td>
			<td>Brand Slug</td>
			<td>
				Action
			</td>
			</tr>
			 <?php
    $users = $init_obj->library->select_data('tbl_brand');
    if(!empty($users))
    {
        $sn=1;
        foreach($users as $user)
        {
               // print_r($user);
               // die();
            ?>
            <tr>
            <td><?php echo $sn++;?></td>
            <td><?php echo $user['brand_name'];?></td>
            <td><?php echo $user['brand_description'];?></td>
            <td><?php echo $user['brand_slug'];?></td>
            <td>
                <a href="<?php echo SITEURL.'/admin/index.php?page=brand-manager&action=edit-brand&brand_id='.$user['brand_id'];?>">Edit</a> &nbsp;|&nbsp;
                <a href="<?php echo SITEURL.'/admin/action.php?action=delete-brand&brand_id='.$user['brand_id'];?>" onclick="return confirm('Are you sure you want to delete?');">Delete</a>

            </td>
        </tr>
        <?php
      }  
    }
    ?>
		
</table>
