<table class="manager-list-table">
    <tr>
        <td colspan="4">
             <?php $init_obj->library->print_session('message'); ?>
            <a href="<?php echo SITEURL. '/admin/index.php?page=user-manager&action=add-users';?>"><input type="button" name="add-users" class="button-secondary float-right" value="add-Users"></a>
        </td>
    </tr>
    <tr>
        <td>Sn</td>
        <td>Username</td>
        <td>Password</td>
        <td>Action</td>
    </tr>
    <?php
    $users = $init_obj->library->select_data('tbl_users');
    if(!empty($users))
    {
        $sn=1;
        foreach($users as $user)
        {
            ?>
            <tr>
            <td><?php echo $sn++;?></td>
            <td><?php echo $user['username'];?></td>
            <td><?php echo $user['password'];?></td>
            <td>
                <a href="<?php echo SITEURL.'/admin/index.php?page=user-manager&action=edit-users&user_id='.$user['user_id'];?>">Edit</a> &nbsp;|&nbsp;
                <a href="<?php echo SITEURL.'/admin/action.php?action=delete-users&user_id='.$user['user_id'];?>" onclick="return confirm('Are you sure you want to delete?');">Delete</a>

            </td>
        </tr>
        <?php
      }  
    }
    ?>
</table>