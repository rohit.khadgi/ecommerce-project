<h3 class="center">Add User</h3>
<form method="post" id="useradd" action="<?php echo SITEURL . '/admin/action.php'; ?>" class="center" style="width:30%;margin: 50px auto;">
    <div class="field-wrap">
         <div class="message"></div>
        <label>Username</label>
        <div class="field"><input type="text" name="username"/></div>
    </div>
    <div class="field-wrap">
        <label>Password</label>
        <div class="field"><input type="password" name="password"/></div>
    </div>
    <div class="field-wrap">
        <label></label>
        <div class="field">
            <input type="submit" name="add_user_submit" value="Add User" class="button-primary"/>
        </div>
    </div>
</form>
