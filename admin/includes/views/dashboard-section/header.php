<!DOCTYPE html>
<html>
    <head>
        <title>Ecommerce Site ADMIN</title>
        <link rel="stylesheet" href="<?php echo SITEURL . '/admin/style/style.css'; ?>">
        <script type="text/javascript" src="<?php echo SITEURL . '/admin/ckeditor/ckeditor.js';?>">
            
        </script>
        <script type="text/javascript" src="<?php echo SITEURL . '/admin/js/jquery.js'; ?>"></script>
            <script type="text/javascript" src="<?php echo SITEURL . '/admin/js/scripts.js'; ?>"></script>

    </head>
    <body>
        <div class="header">
            <h1 class="site-title">Ecommerce Site Admin Panel</h1>
        </div>


        <a  href="/ecommerce/admin/logout.php" style="float: right;"><input type="button" name="logout" value="Logout"></a>
         <a  href="<?php echo SITEURL.'/index.php';?>" style="float: right;"><input type="button" name="ViewSite" value="ViewSite"></a>

        <ul class="menu">
            <li><a href="<?php echo SITEURL . '/admin/index.php?page=user-manager'; ?>">User Manager</a></li>
            <li><a href="<?php echo SITEURL . '/admin/index.php?page=product-manager'?>">Product Manager</a></li>
            <li><a href="<?php echo SITEURL . '/admin/index.php?page=category-manager'?>">Category Manager</a></li>
            <li><a href="<?php echo SITEURL . '/admin/index.php?page=brand-manager'?>">Brand Manager</a></li>   
            <li><a href="<?php echo SITEURL . '/admin/index.php?page=tag-manager'?>">Tag Manager</a></li>                   
             <li><a href="<?php echo SITEURL . '/admin/index.php?page=page-manager'?>">Page Manager</a></li> 
              <li><a href="<?php echo SITEURL . '/admin/index.php?page=order-manager'?>">Order Manager</a></li>  
           
        </ul>