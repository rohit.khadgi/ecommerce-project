<div class="contents">
	
<?php
	if(isset($_GET['page']))
	{
		$manager = $init_obj->library->sanitize_input($_GET['page']);
		$manager_file = "$manager".'.php';
		if(file_exists('includes/views/manager/'.$manager_file)){
			include('includes/views/manager/'.$manager_file);
		}
		else
		{
			?>
			<h2 class="style.css" >
				<?php
				include('includes/views/404.php');

		}
	}
	else
	{
		?>
		<h1 class="site_title" style="text-align: center;">Welcome to Ecommerce Site Admin</h1>
		<?php
	}

?>

</div>