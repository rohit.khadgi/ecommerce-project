<?php include('includes/classes/class-init.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <title>Login Form</title>
    <script type="text/javascript" src="jquery/js/jquery.js"></script>
    <script type="text/javascript" src="jquery/js/scripts.js"></script>
    <style>
input[type=text], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
input[type=password], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=submit] {
    width: 100%;
    background-color:   #FA8072;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color:   #DC143C;
}
input[type=text]:focus {
    border: 3px solid #555;
}

input[type=password]:focus {
    border: 3px solid #555;

div {
    border-radius: 15px;
    background-color: #f2f2f2;
    padding: 20px;
}

</style>
</head>

<style>
input[type=text], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
input[type=password], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=submit] {
    width: 100%;
    background-color:   #FA8072;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color:   #DC143C;
}
input[type=text]:focus {
    border: 3px solid #555;
}

input[type=password]:focus {
    border: 3px solid #555;

div {
    border-radius: 15px;
    background-color: #f2f2f2;
    padding: 20px;
}

</style>
<body>
<div>
    <h1 style="text-align: center; color: #FA8072">Login Form</h1>
    <form method="post" id="login-form" action="<?php echo SITEURL.'/admin/login.php';?>">
        <table align="center">
                <tr>
                <td colspan="2" style="color:red;">
                    <?php
                if(isset($_SESSION['error'])){
                 echo $_SESSION['error'];unset($_SESSION['error']);
            }
            elseif(isset($_SESSION['message'])) {
                echo $_SESSION['message']; unset($_SESSION['message']);
            }

                 ?>
                  <div class="message"></div>
                </td>
            </tr>
      

            <tr><td><strong>Username</strong></td><td><input type="text" name="username"></td></tr>
            <tr><td><strong>Password</strong></td><td><input type="password" name="password"></td></tr>
            <tr><td></td><td><input type="submit" name="login_submit" value="login"></td></tr>
            
        </table>
       
    </form>
    
        

</div>
</body>
</html>